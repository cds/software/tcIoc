#pragma once
#include "stdafx.h"

/** @file ParseUtilConst.h
	Header which includes OPC constants. 
 ************************************************************************/

/** @namespace ParseUtil
	ParseUtil name space 
	@brief Namespace for parsing utilities
 ************************************************************************/
namespace ParseTcXml {

/** @defgroup parsetpyconstxml  XML tpy file constants
 ************************************************************************/
/** @{ */

/// PLC project info
const char* const xmlPlcProjectInfo = "PlcProjectInfo";
/// PLC project tmc info
const char* const xmlPlcProjectTmcInfo = "TcModuleClass";
/// Project info
const char* const xmlProjectInfo = "ProjectInfo";
/// Routing info
const char* const xmlRoutingInfo = "RoutingInfo";
/// Compiler info
const char* const xmlCompilerInfo = "CompilerInfo";
/// ADS info
const char* const xmlAdsInfo = "AdsInfo";
/// Data types
const char* const xmlDataTypes = "DataTypes";
/// Data type
const char* const xmlDataType = "DataType";
/// Modules 
const char* const xmlModules = "Modules";
/// Module 
const char* const xmlModule = "Module";
/// Module data areas 
const char* const xmlDataAreas = "DataAreas";
/// Module data area
const char* const xmlDataArea = "DataArea";
/// Area No
const char* const xmlAreaNo = "AreaNo";
/// Symbols
const char* const xmlSymbols = "Symbols";
/// Symbol
const char* const xmlSymbol = "Symbol";
/// Properties
const char* const xmlProperties = "Properties";
/// Property
const char* const xmlProperty = "Property";
/// Property Application Name
const char* const xmlPropertyApplicationName = "ApplicationName";

/// Compiler version
const char* const xmlCompilerVersion = "CompilerVersion";
/// TwinCAT version
const char* const xmlTwinCATVersion = "TwinCATVersion";
///CPU family
const char* const xmlCpuFamily = "CpuFamily";
/// Net ID
const char* const xmlNetId = "NetId";
/// Port
const char* const xmlPort = "Port";
/// Target name
const char* const xmlTargetName = "TargetName";

/// Name
const char* const xmlName = "Name";
/// Type
const char* const xmlType = "Type";
/// Base type
const char* const xmlBaseType = "BaseType";
/// Namespace
const char* const xmlAttrNamespace = "Namespace";
/// Decoration
const char* const xmlAttrDecoration = "Decoration";
/// Area type
const char* const xmlAttrAreaType = "AreaType";
/// Area type internal
const char* const xmlAttrAreaTypeInternal = "Internal";
/// Area type input
const char* const xmlAttrAreaTypeInputDst = "InputDst";
/// Area type output
const char* const xmlAttrAreaTypeOutputSrc = "OutputSrc";
/// Pointer
const char* const xmlAttrPointer = "Pointer";
/// Pointer to
const char* const xmlAttrPointerTo = "PointerTo";
/// Reference to
const char* const xmlAttrReferenceTo = "ReferenceTo";
/// I Group
const char* const xmlIGroup = "IGroup";
/// I Offset
const char* const xmlIOffset = "IOffset";
/// Bit size
const char* const xmlBitSize = "BitSize";
/// Bit Offset
const char* const xmlBitOffs = "BitOffs";
/// Array info
const char* const xmlArrayInfo = "ArrayInfo";
/// Lower bound
const char* const xmlArrayLBound = "LBound";
/// Elements
const char* const xmlArrayElements = "Elements";
/// Sub item
const char* const xmlSubItem = "SubItem";
/// Fb info
const char* const xmlFbInfo = "FbInfo";
/// Enum info
const char* const xmlEnumInfo = "EnumInfo";
/// Text
const char* const xmlEnumText = "Text";
/// Enum
const char* const xmlEnumEnum = "Enum";
/// Comment
const char* const xmlEnumComment = "Comment";

/// Value
const char* const xmlValue = "Value";
/// Description
const char* const xmlDesc = "Desc";

/// Default
const char* const xmlDefault = "Default";
/// Default value
const char* const xmlDefaultValue = "Value";
/// Default bool
const char* const xmlDefaultBool = "Bool";
/// Default string
const char* const xmlDefaultString = "String";
/// Default date time
const char* const xmlDefaultDateTime = "DateTime";
/// Default Enum text
const char* const xmlDefaultEnumText = "EnumText";
/// Default Subitem
const char* const xmlDefaultSubItem = "SubItem";
/// Default Subitem name
const char* const xmlDefaultSubItemName = "Name";

/// OPC
const char* const opcExport = "opc";
/// OPC property
const char* const opcProp = "opc_prop";
/// OPC bracket
const char* const opcBracket = "[";

/// Substitution 
const char* const xmlSubstitution = "TcSubstitution";
/// Substitution EPICS channel name 
const char* const xmlAlias = "Alias";
/** @} */

}