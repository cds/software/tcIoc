#pragma once
#include "infoPlc.h"

/** @file devInfo.h
	Header which includes classes for Info PLC.
 ************************************************************************/

 /** @namespace InfoPlc
	 InfoPlc Name space
	 @brief Namespace for Info PLC
  ************************************************************************/
namespace InfoPlc {
	
/** This is a class for managing callbacks for info records
    @brief Info PLC callback.
 ************************************************************************/	
class queue_callback_type : public InfoInterface::queue_callback_helper
{
public:
	/** Get the size of the callback ring buffer
		For this function to return a valid value the EPICS
		distribution needs to be patched. Add the following lines:

			epicsShareFunc epicsRingPointerId tcat_callbackQueue (int Priority)
			{
				return (Priority >= 0) && (Priority < NUM_CALLBACK_PRIORITIES) ?
					callbackQueue[Priority].queue : NULL;
			}

		after the declaration of

			static cbQueueSet callbackQueue[NUM_CALLBACK_PRIORITIES];

		in src\ioc\db\callback.c (not needed in EPICS 7)

		@param pri Priority of ring buffer
		@return size of the callback ring buffer 
	 ********************************************************************/
		virtual int get_callback_queue_size(int pri) noexcept override;

	/** Get the used entries in the callback ring buffer
		For this function to return a valid value the EPICS
		distribution needs to be patched. Add the following lines:

		epicsShareFunc epicsRingPointerId tcat_callbackQueue (int Priority)
		{
			return (Priority >= 0) && (Priority < NUM_CALLBACK_PRIORITIES) ?
				callbackQueue[Priority].queue : NULL;
		}

		after the declaration of

		static cbQueueSet callbackQueue[NUM_CALLBACK_PRIORITIES];

		in src\ioc\db\callback.c (not needed in EPICS 7)

		@param pri Priority of ring buffer
		@return used entries in the callback ring buffer 
     ********************************************************************/
	virtual int get_callback_queue_used(int pri) noexcept override;

	/** Get the free entries in the callback ring buffer
		For this function to return a valid value the EPICS
		distribution needs to be patched. Add the following lines:

		epicsShareFunc epicsRingPointerId tcat_callbackQueue (int Priority)
		{
			return (Priority >= 0) && (Priority < NUM_CALLBACK_PRIORITIES) ?
				callbackQueue[Priority].queue : NULL;
		}

		after the declaration of

		static cbQueueSet callbackQueue[NUM_CALLBACK_PRIORITIES];

		in src\ioc\db\callback.c (not needed in EPICS 7)

		@param pri Priority of ring buffer
		@return free entries in the callback ring buffer
	 ********************************************************************/
	virtual int get_callback_queue_free(int pri) noexcept override;

	/** Get the high watermark in the callback ring buffer
		@param pri Priority of ring buffer
		@return high watermark in the callback ring buffer
	 ********************************************************************/
	virtual int get_callback_queue_highwatermark(int pri) noexcept override;
	/** Get the number of overflows in the callback ring buffer
		@param pri Priority of ring buffer
		@return number of overflows in the callback ring buffer 
	 ********************************************************************/
	virtual int get_callback_queue_overflow(int pri) noexcept override;
	/** Reset the overflow count in the callback ring buffer
		@return number of overflows in the callback ring buffer
	 ********************************************************************/
	virtual int set_callback_queue_highwatermark_reset(void) noexcept override;
	
	/// Destructor
	virtual ~queue_callback_type() {};
private:
	/// Default constructor: Sets callback helper in info interface
	queue_callback_type() {
		InfoInterface::set_queue_callback_helper(new queue_callback_type());
	}
};

}
