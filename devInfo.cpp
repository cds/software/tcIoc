#include "devInfo.h"
#if EPICS_VERSION < 7
#include "epicsRingPointer.h"
#endif
#include "callback.h"
#include <iostream>
#if defined(_MSC_VER) && (EPICS_VERSION < 7)
// Exclude rarely-used stuff from Windows headers
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

/** @file devInfo.cpp
	Source for methods that generate EPICs record for the PLC information
 ************************************************************************/

namespace InfoPlc {

//! @cond Doxygen_Suppress

#if defined(_MSC_VER) && (EPICS_VERSION < 7)
/* load_callback_queue variable
 ************************************************************************/
const char* const callback_queue_library = "dbCore.dll";
const char* const callback_queue_symbol = "tcat_callbackQueue";
using callback_queue_func = epicsRingPointerId(__cdecl*)(int);
static std::atomic<bool> callback_queue_init = false;
static std::mutex callback_queue_mux;
static epicsRingPointerId callback_queue[3] = { nullptr, nullptr, nullptr };
static int callback_queue_max[3] = { 0, 0, 0 };
#else
static std::mutex callback_queue_mux;
callbackQueueStats callback_status{};
#endif

#if defined(_MSC_VER) && (EPICS_VERSION < 7)
/* load_callback_queue_func
   Looking for tcat_queuePriorityHigh in dbCore.dll
 ************************************************************************/
static bool load_callback_queue_func() noexcept
{
	bool RunTimeLinkSuccess = false;
	// Use dynamic DLL linking in case of unpatched EPICS base
	HINSTANCE hinstLib{};
	callback_queue_func func = nullptr;
	// Get a handle to the DLL module.
	hinstLib = LoadLibrary(callback_queue_library);
	// If the handle is valid, try to get the function address.
	if (hinstLib != nullptr) {
		func = (callback_queue_func)GetProcAddress(hinstLib, callback_queue_symbol);
		// If the function address is valid, call the function.
		if (func != nullptr) {
			RunTimeLinkSuccess = true;
			callback_queue[priorityLow] = func(priorityLow);
			callback_queue[priorityMedium] = func(priorityMedium);
			callback_queue[priorityHigh] = func(priorityHigh);
		}
		// Free the DLL module.
		FreeLibrary(hinstLib);
	}
	// If unable to call the DLL function, use an alternative.
	if (!RunTimeLinkSuccess) {
		printf("Unable to load callback queue information\n");
	}
	return RunTimeLinkSuccess;
}

/* load_callback_queue_func
 ************************************************************************/
static void check_callback_init() noexcept
{
	// Check if initialized
	if (callback_queue_init) return;
	try {
		// If not get the mutex
		std::lock_guard lock(callback_queue_mux);
		// Check again!
		if (callback_queue_init) return;
		// No initialize
		load_callback_queue_func();
		callback_queue_init = true;
	}
	catch (...) {}
}
#else
/* update_callback
 ************************************************************************/
static void update_callback(bool reset = false) noexcept
{
	try {
		std::lock_guard lock(callback_queue_mux);
		static epicsUInt64 last_access{ 0 };
		epicsUInt64 current = epicsMonotonicGet();
		if (current > last_access + 10000000UL) // 10ms
		{
			last_access = current;
			callbackQueueStatus(reset, &callback_status);
		}
	}
	catch (...) {
		;
	}
}
#endif

/* queue_callback_type::get_callback_queue_size
 ************************************************************************/
int queue_callback_type::get_callback_queue_size(int pri) noexcept
{
	if (!plc::System::get().is_ioc_running()) return -1;
#if EPICS_VERSION < 7
#ifdef _MSC_VER
	check_callback_init();
	return ((pri >= 0) && (pri < NUM_CALLBACK_PRIORITIES) && callback_queue[pri]) ?
		epicsRingPointerGetSize(callback_queue[pri]) : 0;
#else
	return 1;
#endif
#else
	if ((pri < 0) || (pri >= NUM_CALLBACK_PRIORITIES)) return 0;
	update_callback();
	return callback_status.size;
#endif
}

/* queue_callback_type::get_callback_queue_used
 ************************************************************************/
int queue_callback_type::get_callback_queue_used(int pri) noexcept
{
	if (!plc::System::get().is_ioc_running()) return -1;
#if EPICS_VERSION < 7
#ifdef _MSC_VER
	check_callback_init();
	if ((pri < 0) || (pri >= NUM_CALLBACK_PRIORITIES)) return 0;
	const int used = epicsRingPointerGetUsed(callback_queue[pri]);
	if (used > callback_queue_max[pri])  callback_queue_max[pri] = used;
	return used;
#else
	return 0;
#endif
#else
	if ((pri < 0) || (pri >= NUM_CALLBACK_PRIORITIES)) return 0;
	update_callback();
	return callback_status.numUsed[pri];
#endif
}

/* queue_callback_type::get_callback_queue_free
 ************************************************************************/
int queue_callback_type::get_callback_queue_free(int pri) noexcept
{
	if (!plc::System::get().is_ioc_running()) return -1;
#if EPICS_VERSION < 7
#ifdef _MSC_VER	
	check_callback_init();
	return ((pri >= 0) && (pri < NUM_CALLBACK_PRIORITIES) && callback_queue[pri]) ?
		epicsRingPointerGetFree(callback_queue[pri]) : 0;
#else
	return 0;
#endif
#else
	if ((pri < 0) || (pri >= NUM_CALLBACK_PRIORITIES)) return 0;
	update_callback();
	return callback_status.size - callback_status.numUsed[pri];
#endif
}

/* queue_callback_type::get_callback_queue_highwatermark
 ************************************************************************/
int queue_callback_type::get_callback_queue_highwatermark(int pri) noexcept
{
	if (!plc::System::get().is_ioc_running()) return -1;
#if EPICS_VERSION < 7
#ifdef _MSC_VER
	check_callback_init();
	return ((pri >= 0) && (pri < NUM_CALLBACK_PRIORITIES) && callback_queue[pri]) ?
		callback_queue_max[pri] : 0;
#else
	return 0;
#endif
#else
	if ((pri < 0) || (pri >= NUM_CALLBACK_PRIORITIES)) return 0;
	update_callback();
	return callback_status.maxUsed[pri];
#endif
}

/* queue_callback_type::get_callback_queue_overflow
 ************************************************************************/
int queue_callback_type::get_callback_queue_overflow(int pri) noexcept
{
	if (!plc::System::get().is_ioc_running()) return -1;
#if EPICS_VERSION < 7
#ifdef _MSC_VER
	check_callback_init();
	return ((pri >= 0) && (pri < NUM_CALLBACK_PRIORITIES) && callback_queue[pri]) ?
		callback_queue_max[pri] : 0;
#else
	return 0;
#endif
#else
	if ((pri < 0) || (pri >= NUM_CALLBACK_PRIORITIES)) return 0;
	update_callback();
	return callback_status.numOverflow[pri];
#endif
}

/* queue_callback_type::set_callback_queue_highwatermark_reset
 ************************************************************************/
int queue_callback_type::set_callback_queue_highwatermark_reset() noexcept
{
	if (!plc::System::get().is_ioc_running()) return -1;
#if EPICS_VERSION < 7
#ifdef _MSC_VER
	for (auto& i : callback_queue_max) i = 0;
#endif
#else
	update_callback(true);
#endif
	return 0;
}

//! @endcond
}