#include "devTc.h"
#include "ParseTcXml.h"
#include "infoPlc.h"
#include "errlog.h"
#undef va_start
#undef va_end
#include "dbAccess.h"
#include "dbCommon.h"
#include "dbEvent.h"
#include "recGbl.h"
#include "recSup.h"
#include "epicsExport.h"

//int nProcessed = 0;
//clock_t epicsbegin;
//clock_t epicsend;


using namespace std;
using namespace ParseTcXml;
using namespace plc;

/** @file devTc.cpp
	Methods for TwinCAT/ADS device support. 
 ************************************************************************/

namespace DevTc {

/** Initialization function that matches an EPICS record with an internal
	TwinCAT record entry
	@param pEpicsRecord Pointer to EPICS record
	@param pRecord Pointer to a base record
	@return true if successful
	@brief Link TwinCat Record
 ************************************************************************/
static bool linkTcRecord (dbCommon* pEpicsRecord, plc::BaseRecordPtr& pRecord) noexcept
{
	// Let EPICS interface know about size of data
	const TcComms::TCatInterface* const tcat = dynamic_cast<TcComms::TCatInterface*>(pRecord->get_plcInterface());
	if (!tcat) {
		printf("dynamic cast failed, record name is %s.\n", pEpicsRecord->name);
		return false;
	}

	// Create EPICS interface
	DevTc::EpicsInterface* epics = new (std::nothrow) DevTc::EpicsInterface (*pRecord);
	if (!epics) {
		printf("allocation failed, record name is %s.\n", pEpicsRecord->name);
		return false;
	}
	
	// Link EPICS record to EPICS interface
	epics->set_pEpicsRecord(pEpicsRecord);
	// Link EPICS interface to record object
	pRecord->set_userInterface(epics);

	return true;
}

/** Initialization function that matches an EPICS record with an internal
	info record entry
	@param pEpicsRecord Pointer to EPICS record
	@param pRecord Pointer to a base record
	@return true if successful
	@brief Link Info Record
 ************************************************************************/
static bool linkInfoRecord (dbCommon* pEpicsRecord, plc::BaseRecordPtr& pRecord) noexcept
{
	// Let EPICS interface know about size of data
	const InfoPlc::InfoInterface* const info = dynamic_cast<InfoPlc::InfoInterface*>(pRecord->get_plcInterface());
	if (!info) {
		printf("dynamic cast failed, record name is %s.\n", pEpicsRecord->name);
		return false;
	}

	// Create EPICS interface
	DevTc::EpicsInterface* epics = new (std::nothrow) DevTc::EpicsInterface(*pRecord);
	if (!epics) {
		printf("allocation failed, record name is %s.\n", pEpicsRecord->name);
		return false;
	}

	// Link EPICS record to EPICS interface
	epics->set_pEpicsRecord(pEpicsRecord);
	// Link EPICS interface to record object
	pRecord->set_userInterface(epics);

	return true;
}

/** register_devsup::register_devsup
 ************************************************************************/
register_devsup::register_devsup() noexcept
{
	add (tc_regex, linkTcRecord);
	add (info_regex, linkInfoRecord);
}

/** register_devsup::linkRecord
 ************************************************************************/
bool register_devsup::linkRecord (const std::stringcase& inpout, 
	dbCommon* pEpicsRecord, plc::BaseRecordPtr& pRecord) noexcept
{
	if (inpout.empty() ) {
		printf ("Error in inp field for record %s.\n", pEpicsRecord->name);
		return false;
	}

	try {
		std::smatch match;
		for (const auto& i : the_register_devsup.tp_list) {
			if (std::regex_search(inpout, match, i.first)) {
				// Get PLC name from EPICS name string
				BasePLCPtr plcMatch = plc::System::get().find(match[1].str().c_str());
				if (!plcMatch.get()) {
					printf("PLC not found %s.\n", pEpicsRecord->name);
					return false;
				}
				// Link record object to EPICS record
				pRecord = plcMatch->find(inpout);
				if (!pRecord.get()) {
					printf("No PLC record for %s.\n", pEpicsRecord->name);
					return false;
				}
				// Call link function
				return i.second(pEpicsRecord, pRecord);
			}
		}
	}
	catch (...) {}
	printf ("Name doesn't fit regex for %s, link field is %s.\n", 
		pEpicsRecord->name, inpout.c_str());
	return false;
}

/** register_devsup::the_register_devsup
 ************************************************************************/
register_devsup register_devsup::the_register_devsup;


/* EpicsInterface::get_callbackRequestPending
 ************************************************************************/
bool EpicsInterface::get_callbackRequestPending() const noexcept
{
	return (get_record().UserIsDirty());
}

/** Callback complete_io_scan
 ************************************************************************/
void complete_io_scan (EpicsInterface* epics, IOSCANPVT ioscan, int prio) noexcept
{
	epics->ioscan_reset(prio);
}

/* EpicsInterface::ioscan_reset
 ************************************************************************/
void EpicsInterface::ioscan_reset (int bitnum) noexcept
{
	try {
		std::lock_guard guard(ioscanmux);
		std::atomic_fetch_and(&ioscan_inuse, ~(1 << bitnum));
	}
	catch (...) { }
}

/* EpicsInterface::push
 ************************************************************************/
bool EpicsInterface::push() noexcept
{
	//static int skip = 0;
	//static int proc = 0;

	if (isCallback) {
		
		// Generate IO intr request
		if (isPassive) {
			if (callback().priority != priorityHigh) {
				return false;
			}
			if (callbackRequest(&callback()) != 0) {
				return false;
			}
//+			callbackRequestPending = true;
		}
		else {
			try {
				std::lock_guard guard(ioscanmux);
				//if (ioscan_inuse.load() == 0) ++proc; else ++skip;
				std::atomic_store(&ioscan_inuse, scanIoRequest(get_ioscan()));
			}
			catch (...) {}

			/*
			if (ioscan_inuse.load() == 0) {
				std::lock_guard guard(ioscanmux);
				std::atomic_store(&ioscan_inuse, scanIoRequest(get_ioscan()));
				++proc;
				//printf("Processing %s %4.1f %%\n", record.get_name().c_str(), 100.0*(double)skip / (double)(skip + proc));
			}
			else {
				// skip  ioscan due to overload
				++skip;
				//std::lock_guard guard(ioscanmux);
				//std::atomic_store(&ioscan_inuse, 0);
				//if (skip % 100 == 0) {
				//	printf("Skipping %s %4.1f %%\n", record.get_name().c_str(), 100.0*(double)skip / (double)(skip + proc));
				//}
			}
			*/
		}

	}
	return true;
}


/************************************************************************
 * Create and export device support entry tables (DSETs).
 * These tell EPICS about the functions used for TCat device support.
 ************************************************************************/

// ai record
static devTcDefIn<epics_record_enum::aival> aival_record_tc_dset;
static devTcDefIn<epics_record_enum::airval> airval_record_tc_dset;

// aai record
static devTcDefIn<epics_record_enum::aaival> aaival_record_tc_dset;

// bi record
static devTcDefIn<epics_record_enum::bival> bival_record_tc_dset;
static devTcDefIn<epics_record_enum::birval> birval_record_tc_dset;

// longin record
static devTcDefIn<epics_record_enum::longinval> longinval_record_tc_dset;

// int64int record
#if EPICS_VERSION >= 7
static devTcDefIn<epics_record_enum::int64inval> int64inval_record_tc_dset;
#endif

// mbbi record
static devTcDefIn<epics_record_enum::mbbival> mbbival_record_tc_dset;
static devTcDefIn<epics_record_enum::mbbirval> mbbirval_record_tc_dset;

// mbbiDirect record
static devTcDefIn<epics_record_enum::mbbiDirectval> mbbiDirectval_record_tc_dset;
static devTcDefIn<epics_record_enum::mbbiDirectrval> mbbiDirectrval_record_tc_dset;

// stringin record
static devTcDefIn<epics_record_enum::stringinval> stringinval_record_tc_dset;
static devTcDefIn<epics_record_enum::lsival> lsival_record_tc_dset;

// waveform record
static devTcDefWaveformIn<epics_record_enum::waveformval> waveformval_record_tc_dset;


// ao record
static devTcDefOut<epics_record_enum::aoval> aoval_record_tc_dset;
static devTcDefOut<epics_record_enum::aorval> aorval_record_tc_dset;

// aao record
static devTcDefOut<epics_record_enum::aaoval> aaoval_record_tc_dset;

// bo record
static devTcDefOut<epics_record_enum::boval> boval_record_tc_dset;
static devTcDefOut<epics_record_enum::borval> borval_record_tc_dset;

// longout record
static devTcDefOut<epics_record_enum::longoutval> longoutval_record_tc_dset;

// int64out record
#if EPICS_VERSION >= 7
static devTcDefOut<epics_record_enum::int64outval> int64outval_record_tc_dset;
#endif
// mbbo record
static devTcDefOut<epics_record_enum::mbboval> mbboval_record_tc_dset;
static devTcDefOut<epics_record_enum::mbborval> mbborval_record_tc_dset;

// mbboDirect record
static devTcDefOut<epics_record_enum::mbboDirectval> mbboDirectval_record_tc_dset;
static devTcDefOut<epics_record_enum::mbboDirectrval> mbboDirectrval_record_tc_dset;

// stringout record
static devTcDefOut<epics_record_enum::stringoutval> stringoutval_record_tc_dset;
static devTcDefOut<epics_record_enum::lsoval> lsoval_record_tc_dset;

}

extern "C" {
	using namespace DevTc;

// ai record
epicsExportAddress(dset, aival_record_tc_dset); ///< Record processing entry for ai
epicsExportAddress(dset, airval_record_tc_dset); ///< Record processing entry for raw ai 

// aai record
epicsExportAddress(dset, aaival_record_tc_dset); ///< Record processing entry for aai

// bi record
epicsExportAddress(dset, bival_record_tc_dset);  ///< Record processing entry for bi
epicsExportAddress(dset, birval_record_tc_dset);  ///< Record processing entry for raw bi

// longin record
epicsExportAddress(dset, longinval_record_tc_dset);  ///< Record processing entry for longin

// int64in record
#if EPICS_VERSION >= 7
epicsExportAddress(dset, int64inval_record_tc_dset);  ///< Record processing entry for int64in
#endif

// mbbi record
epicsExportAddress(dset, mbbival_record_tc_dset); ///< Record processing entry for mbbi
epicsExportAddress(dset, mbbirval_record_tc_dset); ///< Record processing entry for raw mbbi

// mbbiDirect record
epicsExportAddress(dset, mbbiDirectval_record_tc_dset); ///< Record processing entry for mbbiDirect
epicsExportAddress(dset, mbbiDirectrval_record_tc_dset); ///< Record processing entry for raw mbbiDirect

// stringin record
epicsExportAddress(dset, stringinval_record_tc_dset); ///< Record processing entry for stringin
epicsExportAddress(dset, lsival_record_tc_dset); ///< Record processing entry for lsi (long string)

// waveform record
epicsExportAddress(dset, waveformval_record_tc_dset); ///< Record processing entry for waveform


// ao record
epicsExportAddress(dset, aoval_record_tc_dset); ///< Record processing entry for ao
epicsExportAddress(dset, aorval_record_tc_dset); ///< Record processing entry for raw ao

// aao record
epicsExportAddress(dset, aaoval_record_tc_dset); ///< Record processing entry for aao

// bo record
epicsExportAddress(dset, boval_record_tc_dset); ///< Record processing entry for bo
epicsExportAddress(dset, borval_record_tc_dset); ///< Record processing entry for raw bo

// longout record
epicsExportAddress(dset, longoutval_record_tc_dset); ///< Record processing entry for longout

// int64out record
#if EPICS_VERSION >= 7
epicsExportAddress(dset, int64outval_record_tc_dset);  ///< Record processing entry for int64out
#endif

// mbbo record
epicsExportAddress(dset, mbboval_record_tc_dset); ///< Record processing entry for mbbo
epicsExportAddress(dset, mbborval_record_tc_dset); ///< Record processing entry for raw mbbo

// mbboDirect record
epicsExportAddress(dset, mbboDirectval_record_tc_dset); ///< Record processing entry for mbboDirect
epicsExportAddress(dset, mbboDirectrval_record_tc_dset); ///< Record processing entry for raw mbboDirect

// stringout record
epicsExportAddress(dset, stringoutval_record_tc_dset); ///< Record processing entry for stringout
epicsExportAddress(dset, lsoval_record_tc_dset); ///< Record processing entry for lso (long string)

}
