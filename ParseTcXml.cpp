// This is the implementation of ParseTcXml.
#include "ParseTcXml.h"
#include "ParseUtilConst.h"
#include "ParseTcXmlConst.h"
#include "ParseTpy.h"
#include "ParseTmc.h"
//#define XML_STATIC ///< Static linking
#include "expat.h"
#include <array>

using namespace ParseUtil;

/** @file ParseTcXml.cpp
	Methods to parse TwinCAT tpy file.
 ************************************************************************/

namespace ParseTcXml {	

/* ads_routing_info::get
 ************************************************************************/
bool ads_routing_info::isValid() const noexcept
{
	if (ads_netid.empty() || (ads_port < 0)) {
		return false;
	}
	int end = 0;
	const int num = sscanf_s (ads_netid.c_str(), "%*hhu.%*hhu.%*hhu.%*hhu.%*hhu.%*hhu%n", &end);
	if ((num != 0) || (end != ads_netid.length())) {
		return false;
	}
	return true;
}

/* ads_routing_info::get
 ************************************************************************/
std::stringcase ads_routing_info::get_tc() const
{
	if (!isValid()) {
		return "";
	}
	char buf[256];
	sprintf_s (buf, sizeof (buf), "tc://%s:%i/", ads_netid.c_str(), ads_port);
	buf[255] = 0;
	return buf;
}

/* ads_routing_info::get
 ************************************************************************/
bool ads_routing_info::get (unsigned char& a1, unsigned char& a2, 
							unsigned char& a3, unsigned char& a4, 
							unsigned char& a5, unsigned char& a6) const noexcept
{
	if (!isValid()) {
		return false;
	}
	int end = 0;
	unsigned char b1(0), b2(0), b3(0), b4(0), b5(0), b6 (0); 
	const int num = sscanf_s (ads_netid.c_str(), "%hhu.%hhu.%hhu.%hhu.%hhu.%hhu%n", 
		&b1, &b2, &b3, &b4, &b5, &b6, &end);
	if ((num == 0) || (num == EOF) || (end != (int)ads_netid.length())) {
		return false;
	}
	a1 = b1; a2 = b2; a3 = b3;
	a4 = b4; a5 = b5; a6 = b6;
	return true;
}

/* ads_routing_info::set
 ************************************************************************/
bool ads_routing_info::set_tc (const std::stringcase& s)
{
	char buf[256] = "";
	int p = 0;
	int end = 0;
	const int num = sscanf_s (s.c_str(), " tc://%255s:%i/%n", 
		buf, static_cast<unsigned>(sizeof (buf)), &p, &end);
	if ((num != 2) || (end != s.length())) {
		ads_netid = "";
		ads_port = 0;
		return false;
	}
	buf[255] = 0;
	std::stringcase	ads_netid_old = ads_netid;
	const int ads_port_old = ads_port;
	ads_netid = buf;
	ads_port = p;
	if (!isValid()) {
		ads_netid = ads_netid_old;
		ads_port = ads_port_old;
		return false;
	}
	return true;
}

/* compiler_info::set_cmpl_versionstr
 ************************************************************************/
void compiler_info::set_cmpl_versionstr (const std::stringcase& versionstr) 
{ 
	cmpl_versionstr = versionstr; 
	if (is_cmpl_Valid ()) {
		int v1, v2;
		sscanf_s (cmpl_versionstr.c_str(), "%i.%i.%*s", &v1, &v2);
		if (v2 > 99) {
			cmpl_version = v1 + (double)v2/1000;
		} 
		else if (v2 > 9) {
			cmpl_version = v1 + (double)v2/100;
		}
		else if (v2 > 0) {
			cmpl_version = v1 + (double)v2/10;
		}
		else {
			cmpl_version = 0;
		}
	}
	else {
		cmpl_version = 0;
	}
}

/* compiler_info::is_cmpl_Valid
 ************************************************************************/
bool compiler_info::is_cmpl_Valid() const noexcept
{
	if (cmpl_versionstr.empty()) {
		return false;
	}
	unsigned int v1 = 0;
	unsigned int v2 = 0;
	int end = 0;
	const int num = sscanf_s (cmpl_versionstr.c_str(), "%u.%u.%*s%n", 
						&v1, &v2, &end);
	return (num == 2);
}

/* compiler_info::set_tcat_versionstr
 ************************************************************************/
void compiler_info::set_tcat_versionstr (const std::stringcase& versionstr) 
{ 
	tcat_versionstr = versionstr; 
	if (is_tcat_Valid ()) {
		sscanf_s (tcat_versionstr.c_str(), "%u.%u.%u", &tcat_version_major, 
			&tcat_version_minor, &tcat_version_build);
	}
	else {
		tcat_version_major = 0;
		tcat_version_minor = 0;
		tcat_version_build = 0;
	}
}

/* compiler_info::is_tcat_Valid
 ************************************************************************/
bool compiler_info::is_tcat_Valid() const noexcept
{
	if (tcat_versionstr.empty()) {
		return false;
	}
	unsigned int v1 = 0;
	unsigned int v2 = 0;
	unsigned int v3 = 0;
	int end = 0;
	const int num = sscanf_s (tcat_versionstr.c_str(), "%u.%u.%u%n", 
						&v1, &v2, &v3, &end);
	return (num == 3);
}

/** compareNamesWoNamespace
 ************************************************************************/
static bool compareNamesWoNamespace (const std::stringcase& p1, const std::stringcase& p2)
{
	const size_t pos1 = p1.length();
	const size_t pos2 = p2.length();
	if (pos1 > pos2) {
		return (p1[pos1 - pos2 - 1] == '.') &&
			(p1.compare (pos1 - pos2, std::stringcase::npos, p2) == 0);
	}
	else if (pos2 > pos1) {
		return (p2[pos2 - pos1 - 1] == '.') &&
			(p2.compare (pos2 - pos1, std::stringcase::npos, p1) == 0);
	}
	else {
		return p1 == p2;
	}
}

/* type_map::find
 ************************************************************************/
const type_map::value_type::second_type* 
type_map::find (value_type::first_type id, const std::stringcase& typn) const
{
	const_iterator t = end();
	const_iterator i = type_multipmap::find (id);
	while (i != end()) {
		if (i->first != id) {
			break;
		}
		if (compareNamesWoNamespace (i->second.get_name(), typn)) {
			t = i;
			break;
		}
		++i;
	}
	// fall back to linear search if id == 0, and not found with id == 0
	if ((t == end()) && (id == 0)) {
		i = begin();
		while (i != end()) {
			if (compareNamesWoNamespace(i->second.get_name(), typn)) {
				t = i;
				break;
			}
			++i;
		}
	}
	return (t == end()) ? nullptr : &t->second;
}

/* type_map::patch_tpy_type_decorators
 ************************************************************************/
int type_map::patch_tpy_type_decorators()
{
	int num = 0;
	unsigned int id = 0;
	// simple data type
	std::regex e(R"++(SINT|INT|DINT|LINT|USINT|UINT|UDINT|ULINT|BYTE|WORD|DWORD|LWORD|TIME|TOD|LTIME|DATE|DT|TIME_OF_DAY|DATE_AND_TIME|REAL|LREAL|BOOL|STRING|STRING\([0-9]+\))++");

	for (auto& i : *this)	{
		id = i.second.get_type_decoration();
		// check for zero id, array type and non-trivial type
		if (id != 0) continue;
		if (i.second.get_type_description() != type_enum::arraytype) continue;
		if (std::regex_match(i.second.get_type_name(), e)) continue;
		// find array subtype
		const type_record* t = find (id, i.second.get_type_name());
		if ((t != nullptr) && (t->get_name_decoration() != 0)) {
			i.second.set_type_decoration(t->get_name_decoration());
			++num;
		}
	}
	return num;
}

/* type_map::patch_tpy_type_namespaces
 ************************************************************************/
int type_map::patch_tpy_type_namespaces()
{
	int num = 0;
	for (auto& item : *this) {
		if (patch_tpy_type_namespaces(item.second, true)) ++num;
		if (patch_tpy_type_namespaces(item.second, false)) ++num;
		// check all subitems
		for (auto& subitem : item.second.get_struct_list()) {
			if (patch_tpy_type_namespaces(subitem, false)) ++num;
		}
	}
	return num;
}

/* type_map::patch_tpy_type_namespaces
 ************************************************************************/
bool type_map::patch_tpy_type_namespaces(base_record& item, bool changename)
{
	// check if type name contains namespace
	std::stringcase tn = changename ? item.get_name() : item.get_type_name();
	std::stringcase ns;
	std::stringcase arr;
	size_t pos{};
	// check for array: if present remove it temporarily
	if (tn.starts_with("ARRAY")) {
		pos = tn.find("OF ");
		if (pos != std::string::npos) {
			arr = tn.substr(0, pos + 3);
			tn.erase(0, pos + 3);
		}
	}
	// look a the remainder and remove namespace if there
	pos = tn.find_last_of('.');
	if (pos != std::string::npos) {
		ns = tn.substr(0, pos);
		tn.erase(0, pos + 1);
	}
	// reassemble array
	tn = arr + tn;
	// store back modified type name and namespace
	if (ns.empty()) return false;
	if (changename) {
		item.set_namespace(ns);
		item.set_name(tn);
	}
	else {
		item.set_type_namespace(ns);
		item.set_type_name(tn);
	}
	return true;
}

/* type_map::patch_tmc_type_decorators
 ************************************************************************/
int type_map::patch_tmc_type_decorators()
{
	int num = 0;

	for (auto& i : *this) {
		if (i.second.is_array()) i.second.set_type_description(type_enum::arraytype);
		switch (i.second.get_type_description()) {
		case type_enum::simple:
		case type_enum::arraytype:
		{
			if (patch_tmc_type_decorators(i.second)) ++num;
			break;
		}
		case type_enum::structtype:
		{
			for (auto& subitem : i.second.get_struct_list()) {
				if (patch_tmc_type_decorators(subitem)) ++num;
			}
			break;
		}
		default:
			break;
		}
	}

	// creta new types for arrays
	type_map newtypes;
	for (auto& i : *this) {
		if (i.second.get_type_description() != type_enum::structtype) continue;
		for (auto& subitem : i.second.get_struct_list()) {
			patch_tmc_type_array(subitem, subitem.get_bit_size());
		}
	}

	return num;
}

/* type_map::patch_tmc_type_decorators
 ************************************************************************/
int type_map::patch_tmc_type_decorators(base_record& item)
{
	// simple data type
	std::regex e(R"++(SINT|INT|DINT|LINT|USINT|UINT|UDINT|ULINT|BYTE|WORD|DWORD|LWORD|TIME|TOD|LTIME|DATE|DT|TIME_OF_DAY|DATE_AND_TIME|REAL|LREAL|BOOL|STRING|STRING\([0-9]+\))++");

	if (item.get_type_decoration() != 0 ||
		std::regex_match(item.get_type_name(), e)) return false;
	for (auto& i : *this) {
		if (item.get_type_name() == i.second.get_name() &&
			item.get_type_namespace() == i.second.get_namespace()) {
			item.set_type_decoration(i.first);
			return true;
		}
	}

	return false;
}

/* type_map::patch_tmc_type_decorators
 ************************************************************************/
bool type_map::patch_tmc_type_array(base_record& subitem, unsigned int bitsize)
{
	if (!subitem.is_array()) return false;

	unsigned int id = (unsigned int)size() + 1;
	std::stringcase newtype = "ARRAY [";
	bool first = true;
	for (auto& dim : subitem.get_array_dimensions()) {
		if (!first) newtype += ",";
		first = false;
		newtype += std::to_string(dim.first).c_str();
		newtype += "..";
		newtype += std::to_string(dim.first + dim.second - 1).c_str();
	}
	newtype += "] OF ";
	newtype += subitem.get_type_name();
	// check if it already exists
	bool found = false;
	for (auto& ii : *this) {
		if (newtype == ii.second.get_name() &&
			subitem.get_type_namespace() == ii.second.get_namespace()) {
			id = ii.first;
			found = true;
			break;
		}
	}
	// create new type
	type_record typ;
	if (!found) {
		typ.set_name(newtype);
		typ.set_namespace(subitem.get_type_namespace());
		typ.set_name_decoration(id);
		typ.set_type_decoration(subitem.get_type_decoration());
		typ.set_type_description(type_enum::arraytype);
		typ.set_type_name(subitem.get_type_name());
		typ.set_type_namespace(subitem.get_type_namespace());
		typ.set_bit_size(bitsize);
		typ.get_array_dimensions() = subitem.get_array_dimensions();
		this->insert(std::make_pair(id, typ));
	}
	// patch currrent subitem type
	subitem.set_type_decoration(id);
	subitem.set_type_name(newtype);
	subitem.get_array_dimensions().clear();
	return !found;
}

/// Buffer size of XML parsing
const size_t bufsize = 64 * 2 * BUFSIZ;
/// Buufer type for XML parsing
typedef std::array<char, bufsize> bufftype;

/* tc_xml_file::parse
 ************************************************************************/
tc_xml_file::tc_xml_file(FILE* inp)
{
	parse(inp);
}
/* tc_xml_file::parse
 ************************************************************************/
bool tc_xml_file::parse(FILE* inp, tc_xml_parser_enum psel)
{
	parser_type = tc_xml_parser_enum::NoneOrAny;
	if (!inp) {
		return false;
	}

	// allocate buffer
	std::unique_ptr<bufftype> buf{ new bufftype {} };
	// read some bytes
	size_t len = ::fread(&buf->front(), 1, buf->size(), inp);

	// if unknown parser type, look for first tag
	if (psel == tc_xml_parser_enum::NoneOrAny) {
		psel = detect_parser(&buf->front(), buf->size());
	}
	// if still unknown parser, quit
	if (psel == tc_xml_parser_enum::NoneOrAny) { return false; }

	// get the parser
	std::unique_ptr <tc_xml_parser> parser{ get_parser(psel) };
	// call setup
	if (!parser->parse_setup()) { return false; };

	// keep reading data and parse
	do {
		if (!parser->parse_work(&buf->front(), (int)len, len < bufsize)) { return false; }
		len = ::fread(&buf->front(), 1, buf->size(), inp);
	} while (len != 0);
	parser_type = psel;

	// call finish
	return parser->parse_finish();
}

/* tc_xml_file::parse
 ************************************************************************/
bool tc_xml_file::parse(const char* p, int len, tc_xml_parser_enum psel)
{
	parser_type = tc_xml_parser_enum::NoneOrAny;
	if (!p || (len < 0)) {
		return false;
	}

	// if unknown parser type, look for first tag
	if (psel == tc_xml_parser_enum::NoneOrAny) {
		psel = detect_parser(p, len);
	}
	// if still unknown parser, quit
	if (psel == tc_xml_parser_enum::NoneOrAny) { return false; }

	// get the parser
	std::unique_ptr <tc_xml_parser> parser{ get_parser(psel) };
	// call setup
	if (!parser->parse_setup()) { return false; };

    if (!parser->parse_work(p, len, true)) { return false; }
	parser_type = psel;

	// call finish
	return parser->parse_finish();
}

/* tc_xml_file::parse
   Get a parser of given type
 ************************************************************************/
tc_xml_parser* tc_xml_file::get_parser(tc_xml_parser_enum psel)
{
	switch (psel) {
	default:
	case tc_xml_parser_enum::NoneOrAny:
		return nullptr;
	case tc_xml_parser_enum::TmcFile:
		return new tmc_parser(project_info, sym_list, type_list);
		break;
	case tc_xml_parser_enum::TpyFile:
		return new tpy_parser(project_info, sym_list, type_list);
		break;
	}
}

/// Get a parser by looking for the first XML tag in the given buffer
tc_xml_parser_enum tc_xml_file::detect_parser(const char* start, size_t len)
{
	std::regex findtag{ R"(<(\w+)[^>]*>)" };
	std::cmatch res;
	auto tags_begin = std::regex_iterator(start, start + len, findtag);
	auto tags_end = std::cregex_iterator();
	if (tags_begin != tags_end) {
		const auto& match = *tags_begin;
		std::string s = match.str();
		if (s.starts_with("<PlcProjectInfo")) { return tc_xml_parser_enum::TpyFile; }
		else if (s.starts_with("<TcModuleClass")) { return tc_xml_parser_enum::TmcFile; }
	}
	return tc_xml_parser_enum::NoneOrAny;
}

}