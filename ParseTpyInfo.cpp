#include "stdafx.h"
#include "ParseTcXml.h"

using namespace std;
using namespace ParseTcXml;
using namespace ParseUtil;


/** @file ParseTpyInfo.cpp
	Source file for tpy parsing methods.
 ************************************************************************/

/** Symbol processing
    @brief Symbol processing
 ************************************************************************/
class syminfo_processing {
public:
	/// Constructor
	explicit syminfo_processing (FILE* outfile = 0, bool atomic = true) noexcept
		: outf (outfile ? outfile : stdout), firstline (true) {}
	/// Process
	bool operator() (const process_arg& arg) const noexcept;
protected:
	/// Ouptut file
	FILE*		outf;
	/// Firstline?
	mutable bool firstline;
};

bool syminfo_processing::operator() (const process_arg& arg) const noexcept
{
	if (firstline) {
		firstline = false;
		fprintf (outf, " Basic    IGroup IOffset    Size Type                    Name\n");
	}
	// atomic?
	if (arg.is_atomic()) {
		fprintf (outf, " ");
	}
	else {
		fprintf (outf, "+");
	}
	// Print type (7 chars)
	switch (arg.get_process_type()) 
	{
	// Numeral type
	case process_type_enum::pt_int:
		fprintf (outf, "int    ");
		break;
	// Floating point type
	case process_type_enum::pt_real:
		fprintf (outf, "real   ");
		break;
	// Logic type
	case process_type_enum::pt_bool:
		fprintf (outf, "bool   ");
		break;
	// String type
	case process_type_enum::pt_string:
		fprintf (outf, "string ");
		break;
	// Enumerated type
	case process_type_enum::pt_enum:
		fprintf (outf, "enum   ");
		break;
	// Binary type
	case process_type_enum::pt_binary:
		fprintf (outf, "binary ");
		break;
	// Invalid type
	case process_type_enum::pt_invalid:
	default:
		fprintf (outf, "?      ");
		break;
	}

	// Print memory start address and size (24 chars)
	const process_arg_tc* parg = dynamic_cast<const process_arg_tc*>(&arg);
	if (parg) {
		fprintf(outf, " %7d %7d %7d", parg->get_igroup(), parg->get_ioffset(), (int)(parg->get_bytesize()));
	}

	// Print type and varname
	std::stringcase tn = arg.get_type_name();
	std::stringcase arr;
	size_t pos{};
	if (tn.starts_with("ARRAY")) {
		pos = tn.find("OF ");
		if (pos != std::string::npos) {
			arr = tn.substr(0, pos + 3);
			tn.erase(0, pos + 3);
		}
	}
	pos = tn.find_last_of('.');
	if (pos != std::string::npos) {
		tn.erase(0, pos + 1);
	}
	tn = arr + tn;
	fprintf (outf, " %-23s %-s\n", tn.c_str(), arg.get_name().c_str()); 

	return true;
}


/** Main program for tpyinfo
    @brief tpyinfo
 ************************************************************************/
int main (int argc, const char *argv[])
{
	int			help = 0;
	stringcase	prefix;
	stringcase	inpfilename;
	stringcase	outfilename;
	tc_xml_file	tpyfile;
	bool		argp[100] = {false};

	// command line parsing
	if (argc > 100) argc = 100;
	tpyfile.getopt (argc, argv, argp);
	for (int i = 1; i < argc; ++i) {
		stringcase arg (argv[i] ? argv[i] : "");
		stringcase next (i + 1 < argc && argv[i+1] ? argv[i+1] : "");
		if (arg == "-h" || arg == "/h") {
			help = 1;
		}
		else if ((arg == "-p" || arg == "/p") && i + 1 < argc) {
			prefix = next;
			i += 1;
		}
		else if ((arg == "-i" || arg == "/i") && i + 1 < argc) {
			inpfilename = next;
			i += 1;
		}
		else if ((arg == "-o" || arg == "/o") && i + 1 < argc) {
			outfilename = next;
			i += 1;
		}
		else {
			if (!argp[i]) help = 2;
		}
	}
	if (help) {
		printf ("Usage: tpyinfo [opt] [-p 'prefix'] -i 'tpyfile' -o 'outfile'\n"
			"       Displays information about the individual symbols of a TwinCAT tpy file.\n"
			"       -ea includes all symbols rather than the ones available by opc\n"
			"       -ps only output atomic types\n"
			"       -pc only output structured types\n"
			"       -ns suppress string variables\n"
			"       -p 'prefix' Add a prefix to the variable name\n"
			"       -i 'tpyfile' input file name (stdin when omitted)\n"
		    "       -o 'outfile' output listing (stdout when omitted)\n");
		if (help == 2) return 1; else return 0;
	}

	// open input file
	FILE* inpf = stdin;
	FILE* outf = stdout;
	if (!inpfilename.empty()) {
		if (fopen_s(&inpf, inpfilename.c_str(), "r")) {
			fprintf (stderr, "Failed to open input %s.\n", inpfilename.c_str());
			return 1;
		}
	}
	// open output file
	if (!outfilename.empty()) {
		if (fopen_s(&outf, outfilename.c_str(), "w")) {
			fprintf (stderr, "Failed to open output %s.\n", outfilename.c_str());
			return 1;
		}
	}

	// parse tpy file
	clock_t t1 = clock();
	if (!tpyfile.parse (inpf)) {
		fprintf (stderr, "Unable to parse %s\n", inpfilename.c_str());
		return 1;
	}

	// work through the symbol list
	clock_t t2 = clock();
	syminfo_processing syminfo(outf);
	if (!tpyfile.process_symbols (syminfo, prefix)) {
		fprintf (stderr, "Unable to parse %s\n", inpfilename.c_str());
		return 1;
	}
	clock_t t3 = clock();
	fprintf (stderr, "Time to parse file %g sec, time to build list %g sec.\n", 
		static_cast<double>((int64_t)t2 - (int64_t)t1)/CLOCKS_PER_SEC, 
		static_cast<double>((int64_t)t3 - (int64_t)t2)/CLOCKS_PER_SEC);

	// close files
	if (!inpfilename.empty()) {
		fclose (inpf);
	}
	if (!outfilename.empty()) { 
		fclose (outf);
	}
	return 0;
}
