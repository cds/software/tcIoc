#pragma once
#include "stdafx.h"
#include "ParseUtil.h"
#include "ParseTcXml.h"
#include <expat.h>

/** @file ParseTpy.h
	Header which includes classes to parse a TwinCAT tpy file. 
	It includes "ParseUtil.h" and "ParseTcXml.h"
 ************************************************************************/

/** @namespace ParseTcXml
	ParseTcXml name space 
 	@brief Namespace for parsing
************************************************************************/
namespace ParseTcXml {

/** @defgroup parsetpyopc TwinCAT tpy file parser
 ************************************************************************/
/** @{ */


/** This structure keeps track of the parser information.
	@brief Parser information
 ************************************************************************/
	class tpy_parserinfo
	{
	public:
		/// Constructor
		tpy_parserinfo(project_record& p, symbol_list& s, type_map& t) noexcept
			: ignore(0), projects(false), routing(0), compiler(0), types(0),
			symbols(0), name_parse(0), type_parse(0), opc_cur(0),
			opc_parse(0), opc_cdata(0), igroup_parse(0),
			ioffset_parse(0), bitsize_parse(0),
			bitoffs_parse(0), array_parse(0), enum_parse(0),
			struct_parse(0), fb_parse(0),
			sym_list(&s), type_list(&t), project_info(&p) {
		}

		/// Get symbol list
		symbol_list& get_symbols() noexcept { return *sym_list; }
		/// Get type list
		type_map& get_types() noexcept { return *type_list; }
		/// Get project information
		project_record& get_projectinfo() noexcept { return *project_info; }

		/// Initialze temporary parser info
		void init();
		/// Get type of parsed object
		type_enum get_type_description() const noexcept;

		/// the very top of the xml tag hierarchy (not within any tag)
		bool verytop() const noexcept {
			return !projects && !ignore && !routing && !compiler && !types && !symbols;
		}
		/// the top of the xml tag hierarchy (within the PlcProjectInfo tag)
		bool top() const noexcept {
			return projects && !ignore && !routing && !compiler && !types && !symbols;
		}

		/// ignore elements during parsing (with level)
		int				ignore;
		/// parsing withing PlcProjectInfo tag
		bool			projects;
		/// parsing within Routing tag (1) or AdsInfo (2), Net ID (3), Port (4), Target name (5)
		int				routing;
		/// parsing within compiler tag (1) or compiler version (2), Twincat version (3), CPU family (4)
		int				compiler;
		/// parsing within DataTypes tag (1) or DataType tag (2)
		int				types;
		/// parsing within Symbols tag (1) or Symbol tag (2)
		int				symbols;

		/// temporary symbol information during parsing
		symbol_record	sym;
		/// temporary type information during parsing
		type_record		rec;

		/// level indicator for type name parsing
		/// level indicators in general: 
		///		0 - not encountered, 
		///		1 - parsed,
		///		2 - currently processing,
		///		3 - further sub tag parsing
		int				name_parse;
		/// level indicator for type parsing
		int				type_parse;
		/// Pointer to current opc list (types only)
		ParseUtil::opc_list* opc_cur;
		/// temporary opc element
		ParseUtil::property_el	opc_prop;
		/// level indicator for opc element
		int				opc_parse;
		/// temporary data string for parsed opc data
		std::stringcase	opc_data;
		/// temporary cdata indicator
		int				opc_cdata;
		/// level indicator for IGroup
		int				igroup_parse;
		/// level indicator for IOffset
		int				ioffset_parse;
		/// level indicator for BitSize
		int				bitsize_parse;
		/// level indicator for BitOffs
		int				bitoffs_parse;
		/// temporary data string for parsed igroup/ioffset/bitsize/bitoffs
		std::stringcase	data;

		/// level indicator for array info parsing
		int				array_parse;
		/// temporary data string for parsed array info
		std::stringcase	array_data;
		/// temporary array dimension element
		dimension		array_bounds;
		/// level indicator for enum parsing
		int				enum_parse;
		/// temporary data string for parsed enum data
		std::stringcase	enum_data;
		/// temporary enum element
		enum_pair		enum_element;
		/// temporary enum comment
		std::stringcase	enum_comment;
		/// level indicator for struct parsing
		int				struct_parse;
		/// temporary structure element
		item_record		struct_element;
		/// level indicator for function block parsing
		int				fb_parse;

	protected:
		/// pointer to symbol list
		symbol_list* sym_list;
		/// pointer to type list
		type_map* type_list;
		/// pointer to project info
		project_record* project_info;

	private:
		/// Default constructor
		tpy_parserinfo() = delete;
	};


	/** This structure keeps track of the parser information.
		@brief Parser information
	 ************************************************************************/
	class tpy_parser : public tc_xml_parser
	{
	public:
		/// Constructor
		tpy_parser (project_record& projecti, symbol_list& syml, type_map& typel)
			: tc_xml_parser(projecti, syml, typel), parser(nullptr),
			info(project_info, sym_list, type_list) {
		}

		/** This function is called at the start  of parsing.
			@brief finish up the parsing
		   */
		virtual bool parse_setup() override;
		/** This function is called with a buffer to parse.
			@brief finish up the parsing
		   */
		virtual bool parse_work(const char* s, int len, bool isFinal) override;
		/** This function is called at the end of parsing.
			@brief finish up the parsing
		   */
		virtual bool parse_finish() override;

	protected:
		/// XML parser
		XML_Parser		parser;
		/// Parser info
		tpy_parserinfo	info;
	};

/** @} */

}

