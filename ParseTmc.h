#pragma once
#include "stdafx.h"
#include "ParseUtil.h"
#include "ParseTcXml.h"
#include <expat.h>

/** @file ParseTmc.h
	Header which includes classes to parse a TwinCAT tmc file. 
	It includes "ParseUtil.h" and "ParseTcXml.h"
 ************************************************************************/

/** @namespace ParseTcXml
	ParseTcXml name space 
 	@brief Namespace for parsing
************************************************************************/
namespace ParseTcXml {

/** @defgroup parsetmcopc TwinCAT tmc file parser
 ************************************************************************/
/** @{ */


/** This structure keeps track of the parser information.
	@brief Parser information
 ************************************************************************/
	class tmc_parserinfo
	{
	public:
		/// Constructor
		tmc_parserinfo(project_record& p, symbol_list& s, type_map& t) noexcept
			: sym_list(&s), type_list(&t), project_info(&p) {
		}

		/// Get symbol list
		symbol_list& get_symbols() noexcept { return *sym_list; }
		/// Get type list
		type_map& get_types() noexcept { return *type_list; }
		/// Get project information
		project_record& get_projectinfo() noexcept { return *project_info; }

		/// Initialze temporary parser info
		void init();
		/// Get type of parsed object
		type_enum get_type_description() const noexcept;

		/// the very top of the xml tag hierarchy (not within any tag)
		bool verytop() const noexcept {
			return !projects && !ignore && !dataarea && !modules && !types && !symbols && !properties;
		}
		/// the top of the xml tag hierarchy (within the TcModuleClass tag)
		bool top() const noexcept {
			return projects && !ignore && !dataarea && !modules && !types && !symbols && !properties;
		}

		/// ignore elements during parsing (with level)
		int				ignore = 0;
		/// parsing withing TcModuleClass tag
		bool			projects = false;
		/// parsing within DataTypes tag (1) or DataType tag (2)
		int				types = 0;
		/// parsing within Modules tag (1) or Module tag (2)
		int				modules = 0;
		/// parsing within DataAreas tag (1) or DataArea tag (2)
		int				dataarea = 0;
		/// parsing within Symbol tag (2)
		int				symbols = 0;
		/// parsing within Properties tag (1) or Property tag (2)
		int				properties = 0;

		/// temporary symbol information during parsing
		symbol_record	sym;
		/// temporary type information during parsing
		type_record		rec;

		/// level indicator for type name parsing
		/// level indicators in general: 
		///		0 - not encountered, 
		///		1 - parsed,
		///		2 - currently processing,
		///		3 - further sub tag parsing
		int				name_parse = 0;
		/// level indicator for type parsing
		int				type_parse = 0;
		/// Pointer to current opc list (types only)
		ParseUtil::opc_list* opc_cur = nullptr;
		/// temporary opc element
		ParseUtil::property_el	opc_prop;
		/// level indicator for opc element
		int				opc_parse = 0;
		/// temporary data string for parsed opc data
		std::stringcase	opc_data;
		/// temporary cdata indicator
		int				opc_cdata = 0;
		/// level indicator for BitSize
		int				bitsize_parse = 0;
		/// level indicator for BitOffs
		int				bitoffs_parse = 0;
		/// igroup number
		unsigned int	igroup = 0x4040;
		/// temporary data string for parsed igroup/ioffset/bitsize/bitoffs
		std::stringcase	data;
		/// level indicator for Value
		int				value_parse = 0;
		/// temporary data string for module property name
		std::stringcase	module_prop_name;
		/// temporary data string for module property value
		std::stringcase	module_prop_value;

		/// level indicator for array info parsing
		int				array_parse = 0;
		/// temporary data string for parsed array info
		std::stringcase	array_data;
		/// temporary array dimension element
		dimension		array_bounds;
		/// level indicator for enum parsing
		int				enum_parse = 0;
		/// temporary data string for parsed enum data
		std::stringcase	enum_data;
		/// temporary enum element
		enum_pair		enum_element;
		/// temporary enum comment
		std::stringcase	enum_comment;
		/// level indicator for struct parsing
		int				struct_parse = 0;
		/// temporary structure element
		item_record		struct_element;
		/// level indicator for function block parsing
		int				fb_parse = 0;
		/// level indicator for default value parsing
		int				default_parse = 0;
		/// temporary data string for parsed default values
		std::stringcase	def_data;
		/// temprary default values
		default_values	defvals;
		/// temprary default value
		basic_default_value_item	defval;

	protected:
		/// pointer to symbol list
		symbol_list* sym_list = nullptr;
		/// pointer to type list
		type_map* type_list = nullptr;
		/// pointer to project info
		project_record* project_info = nullptr;

	private:
		/// Default constructor
		tmc_parserinfo() = delete;
	};


	/** This structure keeps track of the parser information.
		@brief Parser information
	 ************************************************************************/
	class tmc_parser : public tc_xml_parser
	{
	public:
		/// Constructor
		tmc_parser(project_record& projecti, symbol_list& syml, type_map& typel)
			: tc_xml_parser(projecti, syml, typel), parser(nullptr),
			info(project_info, sym_list, type_list) {
		}

		/** This function is called at the start  of parsing.
			@brief finish up the parsing
		   */
		virtual bool parse_setup() override;
		/** This function is called with a buffer to parse.
			@brief finish up the parsing
		   */
		virtual bool parse_work(const char* s, int len, bool isFinal) override;
		/** This function is called at the end of parsing.
			@brief finish up the parsing
		   */
		virtual bool parse_finish() override;

	protected:
		/// XML parser
		XML_Parser		parser;
		/// Parser info
		tmc_parserinfo	info;
	};

/** @} */

}

