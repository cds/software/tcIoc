// This is the implementation of ParseTcXml.
#include "ParseTpy.h"
#include "ParseUtilConst.h"
#include "ParseTcXmlConst.h"
//#define XML_STATIC ///< Static linking
#include "expat.h"

#ifdef XML_LARGE_SIZE
#if defined(XML_USE_MSC_EXTENSIONS) && _MSC_VER < 1400
#define XML_FMT_INT_MOD "I64" ///< Int format
#else
#define XML_FMT_INT_MOD "ll" ///< Int format
#endif
#else
#define XML_FMT_INT_MOD "l" ///< Int format
#endif

using namespace ParseUtil;

/** @file ParseTcXml.cpp
	Methods to parse TwinCAT tpy file.
 ************************************************************************/

namespace ParseTcXml {	

/* Forward declaration of XML callbacks
 ************************************************************************/
static void XMLCALL startElement (void *userData, const char *name, 
	const char **atts);
static void XMLCALL endElement (void *userData, const char *name);
static void XMLCALL startCData (void *userData) noexcept;
static void XMLCALL endCData (void *userData) noexcept;
static void XMLCALL dataElement (void *userData, const char *data, int len);


/* tpy_parserinfo::init
 ************************************************************************/
 void tpy_parserinfo::init() 
 {
	 sym = symbol_record();
	 rec = type_record();
	 name_parse = 0;
	 type_parse = 0;
	 opc_cur = 0;
	 opc_prop = property_el();
	 opc_parse = 0;
	 opc_data = std::stringcase ("");
	 opc_cdata = 0;
	 igroup_parse = 0;
	 ioffset_parse = 0;
	 bitsize_parse = 0;
	 data = std::stringcase ("");
	 array_parse = 0;
	 array_data = std::stringcase ("");
	 array_bounds = dimension (0, 0);
	 enum_parse = 0;
	 enum_data = std::stringcase ("");
	 enum_element = enum_pair (0, std::stringcase (""));
	 struct_parse = 0;
	 struct_element = item_record ();
	 fb_parse = 0;
 }

/* tpy_parserinfo::get_type_description
 ************************************************************************/
 type_enum tpy_parserinfo::get_type_description() const noexcept
 {
	 if (name_parse != 1) return type_enum::unknown;
	 if (array_parse == 1 && type_parse == 1) return type_enum::arraytype;
	 if (enum_parse == 1) return type_enum::enumtype;
	 if (struct_parse == 1 && !fb_parse) return type_enum::structtype;
	 if (struct_parse == 1 &&  fb_parse) return type_enum::functionblock;
	 if (type_parse == 1) return type_enum::simple;
	 return type_enum::unknown;
 }

/* tc_xml_file::parse
 ************************************************************************/
bool tpy_parser::parse_setup ()
{
	parser = XML_ParserCreate (NULL);
	XML_SetUserData (parser, &info);
	XML_SetElementHandler (parser, startElement, endElement);
	XML_SetCdataSectionHandler (parser, startCData, endCData);
	XML_SetCharacterDataHandler (parser, dataElement);
	return true;
}

/* tc_xml_file::parse
 ************************************************************************/
bool tpy_parser::parse_work(const char* p, int len, bool isFinal)
{
	if (!p || (len < 0)) {
		return false;
	}
	// read data and parse
	if (XML_Parse (parser, p, len, isFinal) == XML_Status::XML_STATUS_ERROR) {
		fprintf (stderr,
			"%s at line %" XML_FMT_INT_MOD "u\n",
			XML_ErrorString (XML_GetErrorCode (parser)),
			XML_GetCurrentLineNumber (parser));
		return false;
	}
	return true;
}

/* tc_xml_file::parse_finish
 ************************************************************************/
bool tpy_parser::parse_finish ()
{
	// Finish up
	XML_ParserFree(parser);
	parser = nullptr;

	// patch missing type decorators
	int num = type_list.patch_tpy_type_decorators();
	if (num > 0) {
		// fprintf(stderr, "Patching %d type decorators\n", num);
	}
	// patch type namespaces in type list
	num = type_list.patch_tpy_type_namespaces();
	// patch type namespaces in sym list
	for (auto& i : sym_list) {
		if (type_list.patch_tpy_type_namespaces(i, false)) ++num;
	}

	// set plc name in opc
	std::stringcase tcname = project_info.get_tc();
	if (project_info.isValid()) {
		for (auto& sym : sym_list) {
			sym.get_opc().add (property_el (OPC_PROP_PLCNAME, tcname));
		}
	}
	//// patch export all
	//if (get_export_all()) {
	//	for (auto& sym : sym_list) {
	//		sym.get_opc().set_opc_state(ParseUtil::opc_enum::publish);
	//	}
	//}
	return true;
}


/************************************************************************/
/* XML Parsing
 ************************************************************************/

/** XML get decoration number from attribute
 ************************************************************************/
static bool get_decoration (const char **atts, unsigned int& decoration)
{
	for (const char** pp = atts; pp && pp[0] && pp[1]; pp += 2) {
		std::stringcase a (pp[0]);
		if (a.compare (xmlAttrDecoration) == 0) {
			decoration = strtol (pp[1], NULL, 16);
			return true;
		}
	}
	return false;
}

/** XML get pointer from attribute
 ************************************************************************/
static pointerref_enum get_pointer (const char **atts)
{
	for (const char** pp = atts; pp && pp[0] && pp[1]; pp += 2) {
		std::stringcase a (pp[0]);
		if (a.compare (xmlAttrPointer) == 0) {
			std::stringcase val (pp[1]);
			if ((val == "true") || (val == "t") || (val == "1")) return pointerref_enum::pointer;
		}
	}
	return pointerref_enum::No;
}
   
/* XML start element function callback
 ************************************************************************/
static void XMLCALL startElement (void *userData, const char *name, 
	const char **atts)
{
	tpy_parserinfo*	pinfo = (tpy_parserinfo*) userData;
	if (pinfo->ignore) {
		++pinfo->ignore;
		return;
	}
	std::stringcase n (name ? name : "");

	// Parse PLC project information
	if (pinfo->verytop() && (n.compare (xmlPlcProjectInfo) == 0)) {
		pinfo->projects = true;
	}

	// Parse routing information
	else if (n.compare (xmlRoutingInfo) == 0) {
		if (pinfo->top()) {
			++pinfo->routing;
		}
		else ++pinfo->ignore;
	}
	else if (n.compare (xmlAdsInfo) == 0) {
		if (pinfo->routing == 1) {
			++pinfo->routing;
		}
		else ++pinfo->ignore;
	}
	else if (pinfo->routing >= 2) {
		// Net Id
		if (n.compare (xmlNetId) == 0 && (pinfo->routing == 2)) {
			pinfo->data = std::stringcase ("");
			pinfo->routing = 3;
		}
		// Port
		else if (n.compare (xmlPort) == 0 && (pinfo->routing == 2)) {
			pinfo->data = std::stringcase ("");
			pinfo->routing = 4;
		}
		// Target name
		else if (n.compare (xmlTargetName) == 0 && (pinfo->routing == 2)) {
			pinfo->data = std::stringcase ("");
			pinfo->routing = 5;
		}
	}

	// Parse compiler information
	else if (n.compare (xmlCompilerInfo) == 0) {
		if (pinfo->top()) {
			++pinfo->compiler;
		}
		else ++pinfo->ignore;
	}
	else if (pinfo->compiler >= 1) {
		// Net Id
		if (n.compare (xmlCompilerVersion) == 0 && (pinfo->compiler == 1)) {
			pinfo->data = std::stringcase ("");
			pinfo->compiler = 2;
		}
		// Port
		else if (n.compare (xmlTwinCATVersion) == 0 && (pinfo->compiler == 1)) {
			pinfo->data = std::stringcase ("");
			pinfo->compiler = 3;
		}
		// CPU Family
		else if (n.compare (xmlCpuFamily) == 0 && (pinfo->compiler == 1)) {
			pinfo->data = std::stringcase ("");
			pinfo->compiler = 4;
		}
	}

	// Parse symbol information
	else if (n.compare (xmlSymbols) == 0) {
		if (pinfo->top()) {
			++pinfo->symbols;
		}
		else ++pinfo->ignore;
	}
	else if (n.compare (xmlSymbol) == 0) {
		if (pinfo->symbols == 1) {
			++pinfo->symbols;
			pinfo->init();
		}
		else ++pinfo->ignore;
	}
	else if (pinfo->symbols == 2) {
		// name of symbol
		if (n.compare (xmlName) == 0 && !pinfo->name_parse && 
			!pinfo->opc_parse) {
				pinfo->name_parse = 2;
		}
		// type of symbol
		else if (n.compare (xmlType) == 0 && !pinfo->type_parse) {
			pinfo->type_parse = 2;
			unsigned int decor = 0;
			get_decoration (atts, decor);
			pinfo->sym.set_type_decoration (decor);
			pinfo->sym.set_type_pointer (get_pointer (atts));
		}
		// opc properties
		else if (n.compare (xmlProperties) == 0 && !pinfo->opc_parse &&
			pinfo->name_parse <= 1 && pinfo->type_parse <= 1) {
				pinfo->opc_parse = 1;
		}
		// opc property
		else if (n.compare (xmlProperty) == 0 && pinfo->opc_parse == 1) {
			pinfo->opc_parse = 2;
			pinfo->opc_prop = property_el ();
		}
		// opc property name
		else if (n.compare (xmlName) == 0 && pinfo->opc_parse == 2) {
			pinfo->opc_parse = 3;
			pinfo->opc_data = "";
			pinfo->opc_cdata = 1;
		}
		// opc property value
		else if (n.compare (xmlValue) == 0 && pinfo->opc_parse == 2) {
			pinfo->opc_parse = 4;
			pinfo->opc_data = "";
			pinfo->opc_cdata = 1;
		}
		// igroup
		else if (n.compare (xmlIGroup) == 0 && !pinfo->igroup_parse) {
			pinfo->igroup_parse = 2;
			pinfo->data = std::stringcase ("");
		}
		// ioffset
		else if (n.compare (xmlIOffset) == 0 && !pinfo->ioffset_parse) {
			pinfo->ioffset_parse = 2;
			pinfo->data = std::stringcase ("");
		}
		// bitsize
		else if (n.compare (xmlBitSize) == 0 && !pinfo->bitsize_parse) {
			pinfo->bitsize_parse = 2;
			pinfo->data = std::stringcase ("");
		}
		else {
			++pinfo->ignore;
		}
	}

	// Parse data type information
	else if (n.compare (xmlDataTypes) == 0) {
		if (pinfo->top()) {
			++pinfo->types;
		}
		else ++pinfo->ignore;
	}
	else if (n.compare (xmlDataType) == 0) {
		if (pinfo->types == 1) {
			++pinfo->types;
			pinfo->init();
		}
		else ++pinfo->ignore;
	}
	else if (pinfo->types == 2) {
		// name of type
		if (n.compare (xmlName) == 0 && !pinfo->name_parse && 
			(pinfo->struct_parse <= 1) && !pinfo->opc_cur) {
				pinfo->name_parse = 2;
				unsigned int decor = 0;
				get_decoration (atts, decor);
				pinfo->rec.set_name_decoration (decor);
		}
		// right hand type
		else if (n.compare (xmlType) == 0 && !pinfo->type_parse && 
			pinfo->struct_parse <= 1) {
				pinfo->type_parse = 2;
				unsigned int decor = 0;
				get_decoration (atts, decor);
				pinfo->rec.set_type_decoration (decor);
		}
		// bit size
		else if (n.compare (xmlBitSize) == 0 && !pinfo->bitsize_parse &&
			pinfo->struct_parse <= 1) {
				pinfo->bitsize_parse = 2;
				pinfo->data = std::stringcase ("");
		}
		// array info
		else if (n.compare (xmlArrayInfo) == 0) {
			pinfo->array_parse = 2;
			pinfo->array_bounds = dimension (0, 0);
		}
		// lower array bound
		else if (n.compare (xmlArrayLBound) == 0 && 
			pinfo->array_parse == 2) {
				pinfo->array_parse = 3;
				pinfo->array_data = std::stringcase ("");
		}
		// array elements
		else if (n.compare (xmlArrayElements) == 0 && 
			pinfo->array_parse == 2) {
				pinfo->array_parse = 3;
				pinfo->array_data = std::stringcase ("");
		}
		// enum
		else if (n.compare (xmlEnumInfo) == 0) {
			pinfo->enum_parse = 2;
			pinfo->enum_element = enum_pair (0, "");
			pinfo->enum_comment.clear();
		}
		// enum tag
		else if (n.compare (xmlEnumEnum) == 0 && 
			pinfo->enum_parse == 2) {
				pinfo->enum_parse = 3;
				pinfo->enum_data = std::stringcase ("");
		}
		// enum text
		else if (n.compare (xmlEnumText) == 0 && 
			pinfo->enum_parse == 2) {
				pinfo->enum_parse = 3;
				pinfo->enum_data = std::stringcase ("");
		}
		// enum comment
		else if (n.compare (xmlEnumComment) == 0 && 
			pinfo->enum_parse == 2) {
				pinfo->enum_parse = 3;
				pinfo->enum_data = std::stringcase ("");
		}
		// structure
		else if (n.compare (xmlSubItem) == 0) {
			pinfo->struct_parse = 2;
			pinfo->struct_element = item_record();
		}
		// structure element name
		else if (n.compare (xmlName) == 0 && pinfo->struct_parse == 2 && 
			!pinfo->opc_cur) {
				pinfo->struct_parse = 3;
		}
		// structure element type
		else if (n.compare (xmlType) == 0 && pinfo->struct_parse == 2 && 
			!pinfo->opc_cur) {
				pinfo->struct_parse = 4;
				unsigned int decor = 0;
				get_decoration (atts, decor);
				pinfo->struct_element.set_type_decoration (decor);
		}
		// structure element bitsize
		else if (n.compare (xmlBitSize) == 0 && pinfo->struct_parse == 2 && 
			!pinfo->opc_cur) {
				pinfo->struct_parse = 5;
				pinfo->data = std::stringcase ("");
		}
		// structure element bitoffs
		else if (n.compare (xmlBitOffs) == 0 && pinfo->struct_parse == 2 && 
			!pinfo->opc_cur) {
				pinfo->struct_parse = 5;
				pinfo->data = std::stringcase ("");
		}
		// function block
		else if (n.compare (xmlFbInfo) == 0) {
			pinfo->fb_parse = 1;
			++pinfo->ignore;
		}
		// opc properties
		else if (n.compare (xmlProperties) == 0 && !pinfo->opc_cur &&
			pinfo->name_parse <= 1 &&
			pinfo->type_parse <= 1 &&
			pinfo->enum_parse <= 1 &&
			pinfo->array_parse <= 1 &&
			pinfo->struct_parse <= 2) {
				pinfo->opc_parse = 1;
				if (pinfo->struct_parse == 2) 
					pinfo->opc_cur = &pinfo->struct_element.get_opc();
				else pinfo->opc_cur = &pinfo->rec.get_opc();
		}
		// opc property
		else if (n.compare (xmlProperty) == 0 && pinfo->opc_cur &&
			pinfo->opc_parse == 1) {
				pinfo->opc_parse = 2;
				pinfo->opc_prop = property_el ();
		}
		// opc property name
		else if (n.compare (xmlName) == 0 && pinfo->opc_cur &&
			pinfo->opc_parse == 2) {
				pinfo->opc_parse = 3;
				pinfo->opc_data = "";
				pinfo->opc_cdata = 1;
		}
		// opc property value
		else if (n.compare (xmlValue) == 0 && pinfo->opc_cur &&
			pinfo->opc_parse == 2) {
				pinfo->opc_parse = 4;
				pinfo->opc_data = "";
				pinfo->opc_cdata = 1;
		}
		else {
			++pinfo->ignore;
		}
	}
	else {
		++pinfo->ignore;
	}
}

/* XML end element function callback
 ************************************************************************/
static void XMLCALL endElement (void *userData, const char *name)
{
	tpy_parserinfo*	pinfo = (tpy_parserinfo*) userData;
	if (pinfo->ignore) {
		--pinfo->ignore;
		return;
	}
	std::stringcase n (name ? name : "");

	// parsing PLC project information
	if (n.compare (xmlPlcProjectInfo) == 0) {
		if (pinfo->top()) pinfo->projects = false;
	}

	// Parse routing information
	else if (n.compare (xmlRoutingInfo) == 0) {
		if (pinfo->routing == 1) --pinfo->routing;
	}
	else if (n.compare (xmlAdsInfo) == 0) {
		if (pinfo->routing == 2) --pinfo->routing;
	}
	else if (pinfo->routing >= 2) {
		// Net Id
		if (n.compare (xmlNetId) == 0 && (pinfo->routing == 3)) {
			pinfo->routing = 2;
			trim_space (pinfo->data);
			pinfo->get_projectinfo().set_netid(pinfo->data);
		}
		// Port
		else if (n.compare (xmlPort) == 0 && (pinfo->routing == 4)) {
			pinfo->routing = 2;
			const int num = strtol (pinfo->data.c_str(), NULL, 10);
			pinfo->get_projectinfo().set_port (num);
		}
		// Target name
		else if (n.compare (xmlTargetName) == 0 && (pinfo->routing == 5)) {
			pinfo->routing = 2;
			trim_space (pinfo->data);
			pinfo->get_projectinfo().set_targetname(pinfo->data);
		}
	}

	// Parse compiler information
	else if (n.compare (xmlCompilerInfo) == 0) {
		if (pinfo->compiler == 1) --pinfo->compiler;
	}
	else if (pinfo->compiler >= 1) {
		// compiler version
		if (n.compare (xmlCompilerVersion) == 0 && (pinfo->compiler == 2)) {
			pinfo->compiler = 1;
			trim_space (pinfo->data);
			pinfo->get_projectinfo().set_cmpl_versionstr(pinfo->data);
		}
		// twincat version
		else if (n.compare (xmlTwinCATVersion) == 0 && (pinfo->compiler == 3)) {
			pinfo->compiler = 1;
			trim_space (pinfo->data);
			pinfo->get_projectinfo().set_tcat_versionstr (pinfo->data);
		}
		// CPU family
		else if (n.compare (xmlCpuFamily) == 0 && (pinfo->compiler == 4)) {
			pinfo->compiler = 1;
			trim_space (pinfo->data);
			pinfo->get_projectinfo().set_cpu_family (pinfo->data);
		}
	}

	// parsing symbols
	else if (n.compare (xmlSymbols) == 0) {
		if (pinfo->symbols == 1) --pinfo->symbols;
	}
	else if (n.compare (xmlSymbol) == 0) {
		if (pinfo->symbols == 2) {
			--pinfo->symbols;
			if (!pinfo->sym.get_name().empty()) {
				// pointers are readonly!
				if (pinfo->sym.get_type_pointer() != pointerref_enum::No) {
					pinfo->sym.get_opc().get_properties()[OPC_PROP_RIGHTS] = "1";
				}
				pinfo->get_symbols().push_back (pinfo->sym);
			}
		}
	}
	else if (pinfo->symbols == 2) {
		// parsed a name (trim space)
		if (n.compare (xmlName) == 0 && pinfo->name_parse == 2) {
			pinfo->name_parse = 1;
			trim_space (pinfo->sym.get_name());
		}
		// parsed a type (trim space)
		else if (n.compare (xmlType) == 0 && pinfo->type_parse == 2) {
			pinfo->type_parse = 1;
			trim_space (pinfo->sym.get_type_name());
		}
		// opc properties
		else if (n.compare (xmlProperties) == 0 && pinfo->opc_parse == 1) {
			pinfo->opc_parse = 0;
		}
		// opc property
		else if (n.compare (xmlProperty) == 0 && pinfo->opc_parse == 2) {
			pinfo->opc_parse = 1;
			if (pinfo->opc_prop.first == -1) {
				const int num = strtol (pinfo->opc_prop.second.c_str(), NULL, 10);
				pinfo->sym.get_opc().set_opc_state (num ? opc_enum::publish : opc_enum::silent);
			}
			else if (pinfo->opc_prop.first > 0) {
				pinfo->sym.get_opc().add (pinfo->opc_prop);
			}
		}
		// opc property name
		else if (n.compare (xmlName) == 0 && pinfo->opc_parse == 3) {
			pinfo->opc_parse = 2;
			trim_space (pinfo->opc_data);
			int num = 0;
			if (pinfo->opc_data.compare (opcExport) == 0) num = -1;
			else if (pinfo->opc_data.compare (0, 8, opcProp) == 0) {
				pinfo->opc_data.erase (0, 8);
				trim_space (pinfo->opc_data);
				if (pinfo->opc_data.compare (0, 1, opcBracket) == 0) 
					pinfo->opc_data.erase (0, 1);
				num = strtol (pinfo->opc_data.c_str(), NULL, 10);
			}
			pinfo->opc_prop.first = num;
		}
		// opc property value
		else if (n.compare (xmlValue) == 0 && pinfo->opc_parse == 4) {
			pinfo->opc_parse = 2;
			pinfo->opc_prop.second = pinfo->opc_data;
		}
		// parsed a igroup (convert to integer)
		else if (n.compare (xmlIGroup) == 0 && pinfo->igroup_parse == 2) {
			pinfo->igroup_parse = 1;
			pinfo->sym.set_igroup (strtol (pinfo->data.c_str(), NULL, 10));
		}
		// parsed a ioffset (convert to integer)
		else if (n.compare (xmlIOffset) == 0 && pinfo->ioffset_parse == 2) {
			pinfo->ioffset_parse = 1;
			pinfo->sym.set_ioffset (strtol (pinfo->data.c_str(), NULL, 10));
		}
		// parsed a bitsize (convert to integer)
		else if (n.compare (xmlBitSize) == 0 && pinfo->bitsize_parse == 2) {
			pinfo->bitsize_parse = 1;
			pinfo->sym.set_bytesize (strtol (pinfo->data.c_str(), NULL, 10) / 8);
		}
	}
	// parsing data types
	else if (n.compare (xmlDataTypes) == 0) {
		if (pinfo->types == 1) --pinfo->types;
	}
	else if (n.compare (xmlDataType) == 0) {
		if (pinfo->types == 2) {
			--pinfo->types;
			pinfo->rec.set_type_description (pinfo->get_type_description());
			// remove simple type which reference themselves, ie., discard type aliases
			if ((pinfo->rec.get_type_description() != type_enum::simple) ||
				(pinfo->rec.get_name() != pinfo->rec.get_type_name())) {
				pinfo->get_types().insert (
				type_map::value_type (pinfo->rec.get_name_decoration(), pinfo->rec));
			}
		}
	}
	else if (pinfo->types == 2) {
		// parsed a name (trim space)
		if (n.compare (xmlName) == 0 && pinfo->name_parse == 2) {
			pinfo->name_parse = 1;
			trim_space (pinfo->rec.get_name());
		}
		// parsed a type (trim space)
		else if (n.compare (xmlType) == 0 && pinfo->type_parse == 2) {
			pinfo->type_parse = 1;
			trim_space (pinfo->rec.get_type_name());
		}
		// parsed a bit size
		else if (n.compare (xmlBitSize) == 0 && pinfo->bitsize_parse == 2) {
			pinfo->bitsize_parse = 1;
			pinfo->rec.set_bit_size (strtol (pinfo->data.c_str(), NULL, 10));
		}
		// parsed an array info
		else if (n.compare (xmlArrayInfo) == 0 && pinfo->array_parse == 2) {
			pinfo->array_parse = 1;
			if (pinfo->array_bounds.second > 0) {
				pinfo->rec.get_array_dimensions().push_back (pinfo->array_bounds);
			}
		}
		// lower array bound
		else if (n.compare (xmlArrayLBound) == 0 && 
			pinfo->array_parse == 3) {
				pinfo->array_bounds.first = 
					strtol (pinfo->array_data.c_str(), NULL, 10);
				pinfo->array_parse = 2;
		}
		// array elements
		else if (n.compare (xmlArrayElements) == 0 && 
			pinfo->array_parse == 3) {
				pinfo->array_bounds.second = 
					strtol (pinfo->array_data.c_str(), NULL, 10);
				pinfo->array_parse = 2;
		}
		// parsed an enum info
		else if (n.compare (xmlEnumInfo) == 0 && pinfo->enum_parse == 2) {
			pinfo->enum_parse = 1;
			pinfo->rec.get_enum_list().insert (pinfo->enum_element);
			if (!pinfo->enum_comment.empty() && (pinfo->enum_element.first >= 0) &&
			   (pinfo->enum_element.first < 16)) {
				pinfo->rec.get_opc().get_properties()[OPC_PROP_ZRST+pinfo->enum_element.first] = pinfo->enum_comment;
			}
		}
		// enum tag
		else if (n.compare (xmlEnumEnum) == 0 && 
			pinfo->enum_parse == 3) {
				pinfo->enum_element.first = 
					strtol (pinfo->enum_data.c_str(), NULL, 10);
				pinfo->enum_parse = 2;
		}
		// enum text
		else if (n.compare (xmlEnumText) == 0 && 
			pinfo->enum_parse == 3) {
				trim_space (pinfo->enum_data);
				pinfo->enum_element.second = pinfo->enum_data;
				pinfo->enum_parse = 2;
		}
		// enum comment
		else if (n.compare (xmlEnumComment) == 0 && 
			pinfo->enum_parse == 3) {
				trim_space (pinfo->enum_data);
				pinfo->enum_comment = pinfo->enum_data;
				pinfo->enum_parse = 2;
		}
		// parsed a subitem
		else if (n.compare (xmlSubItem) == 0 && pinfo->struct_parse == 2) {
			pinfo->struct_parse = 1;
			pinfo->rec.get_struct_list().push_back (pinfo->struct_element);
		}
		// subitem name
		else if (n.compare (xmlName) == 0 && pinfo->struct_parse == 3) {
			trim_space (pinfo->struct_element.get_name());
			pinfo->struct_parse = 2;
		}
		// subitem type
		else if (n.compare (xmlType) == 0 && pinfo->struct_parse == 4) {
			trim_space (pinfo->struct_element.get_type_name());
			pinfo->struct_parse = 2;
		}
		// subitem bitsize
		else if (n.compare (xmlBitSize) == 0 && pinfo->struct_parse == 5) {
			pinfo->struct_element.set_bit_size (
				strtol (pinfo->data.c_str(), NULL, 10));
			pinfo->struct_parse = 2;
		}
		// subitem bitoffs
		else if (n.compare (xmlBitOffs) == 0 && pinfo->struct_parse == 5) {
			pinfo->struct_element.set_bit_offset (
				strtol (pinfo->data.c_str(), NULL, 10));
			pinfo->struct_parse = 2;
		}
		// opc properties
		else if (n.compare (xmlProperties) == 0 && pinfo->opc_parse == 1) {
			pinfo->opc_parse = 0;
			pinfo->opc_cur = 0;
		}
		// opc property
		else if (n.compare (xmlProperty) == 0 && pinfo->opc_parse == 2) {
			pinfo->opc_parse = 1;
			if (pinfo->opc_prop.first == -1) {
				const int num = strtol (pinfo->opc_prop.second.c_str(), NULL, 10);
				if (pinfo->opc_cur) 
					pinfo->opc_cur->set_opc_state (num ? opc_enum::publish : opc_enum::silent);
			}
			else if (pinfo->opc_prop.first > 0) {
				if (pinfo->opc_cur) 
					pinfo->opc_cur->get_properties().insert (pinfo->opc_prop);
			}
		}
		// opc property name
		else if (n.compare (xmlName) == 0 && pinfo->opc_parse == 3) {
			pinfo->opc_parse = 2;
			trim_space (pinfo->opc_data);
			int num = 0;
			if (pinfo->opc_data.compare (opcExport) == 0) num = -1;
			else if (pinfo->opc_data.compare (0, 8, opcProp) == 0) {
				pinfo->opc_data.erase (0, 8);
				trim_space (pinfo->opc_data);
				if (pinfo->opc_data.compare (0, 1, opcBracket) == 0) 
					pinfo->opc_data.erase (0, 1);
				num = strtol (pinfo->opc_data.c_str(), NULL, 10);
			}
			pinfo->opc_prop.first = num;
		}
		// opc property value
		else if (n.compare (xmlValue) == 0 && pinfo->opc_parse == 4) {
			pinfo->opc_parse = 2;
			pinfo->opc_prop.second = pinfo->opc_data;
		}
	}
}

/* XML start CData function callback
 ************************************************************************/
static void XMLCALL startCData (void *userData) noexcept
{
	tpy_parserinfo*	pinfo = (tpy_parserinfo*) userData;
	if (pinfo->ignore) {
		return;
	}
	if (pinfo->symbols == 2) {
		// clear opc data
		if (pinfo->opc_parse >= 3) {
			pinfo->opc_data.clear ();
			pinfo->opc_cdata = 1;
		}
	}
	else if (pinfo->types == 2) {
		// clear opc data
		if (pinfo->opc_parse >= 3) {
			pinfo->opc_data.clear ();
			pinfo->opc_cdata = 1;
		}
	}
}

/* XML end CData function callback
 ************************************************************************/
static void XMLCALL endCData (void *userData) noexcept
{
	tpy_parserinfo*	pinfo = (tpy_parserinfo*) userData;
	if (pinfo->ignore) {
		return;
	}
	if (pinfo->symbols == 2) {
		// clear opc data
		if (pinfo->opc_parse >= 3) {
			pinfo->opc_cdata = 0;
		}
	}
	else if (pinfo->types == 2) {
		// clear opc data
		if (pinfo->opc_parse >= 3) {
			pinfo->opc_cdata = 0;
		}
	}
}


/* XML data function callback
 ************************************************************************/
static void XMLCALL dataElement (void *userData, const char *data, int len)
{
	tpy_parserinfo*	pinfo = (tpy_parserinfo*) userData;
	if (pinfo->ignore) {
		return;
	}

	// get data from routing
	if (pinfo->routing >= 2) {
		pinfo->data.append (data, len);
	}
	// get data from compiler
	else if (pinfo->compiler >= 1) {
		pinfo->data.append (data, len);
	}

	// get symbol data
	else if (pinfo->symbols == 2) {
		// append string to name
		if (pinfo->name_parse == 2) {
			pinfo->sym.get_name().append (data, len);
		}
		// append string to type
		else if (pinfo->type_parse == 2) {
			pinfo->sym.get_type_name().append (data, len);
		}
		// append opc data
		else if (pinfo->opc_parse >= 3) {
			if (pinfo->opc_cdata) 
				pinfo->opc_data.append (data, len);
		}
		// append igroup/ioffset/bitsize data
		else if ((pinfo->igroup_parse == 2) ||
			(pinfo->ioffset_parse == 2) ||
			(pinfo->bitsize_parse == 2)) {
				pinfo->data.append (data, len);
		}
	}

	// get data type information
	else if (pinfo->types == 2) {
		// append string to name
		if (pinfo->name_parse == 2) {
			pinfo->rec.get_name().append (data, len);
		}
		// append string to type
		else if (pinfo->type_parse == 2) {
			pinfo->rec.get_type_name().append (data, len);
		}
		// append string to bitdata
		else if (pinfo->bitsize_parse == 2) {
			pinfo->data.append (data, len);
		}
		// append array info data
		else if (pinfo->array_parse == 3) {
			pinfo->array_data.append (data, len);
		}
		// append enum data
		else if (pinfo->enum_parse == 3) {
			pinfo->enum_data.append (data, len);
		}
		// append struct element name
		else if (pinfo->struct_parse == 3) {
			pinfo->struct_element.get_name().append (data, len);
		}
		// append struct element type
		else if (pinfo->struct_parse == 4) {
			pinfo->struct_element.get_type_name().append (data, len);
		}
		// append struct element bitsize
		else if (pinfo->struct_parse == 5) {
			pinfo->data.append (data, len);
		}
		// append struct element bitoffs
		else if (pinfo->struct_parse == 6) {
			pinfo->data.append (data, len);
		}
		// append opc data
		else if (pinfo->opc_parse >= 3) {
			if (pinfo->opc_cdata) 
				pinfo->opc_data.append (data, len);
		}
	}
}

}