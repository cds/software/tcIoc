#####################################################################
# doxygen.ps1
# Author: Daniel Sigg/Maggie Tse
# Date: June 2019
#####################################################################
#
# Powershell script to run doxygen
#
# Requires doxygen
# web: http://doxygen.nl/
# Requires graphviz
# web: http://www.graphviz.org/
#
# Parent directory
$scriptroot = "$PSScriptRoot"
$parent = "$scriptroot\.."
$download = "$parent\Download"
$doxy = "$parent\doxygen\Doxyfile"
$doxytex = "$parent\doxygen\Doxyfile_latex"
$html = "$parent\doxygen\documentation\html"
$latex = "$parent\doxygen\documentation\latex"
# 
$version = "2_4"
#
# set path
$env:Path = "C:\Program Files\doxygen\bin;$env:Path"
$env:Path = "C:\Program Files\Graphviz\bin;$env:Path"

Remove-Item -Recurse -Force $html
Remove-Item -Recurse -Force $latex
cd $parent
doxygen $doxytex 
Compress-Archive -Path $html -DestinationPath $download\html_${version}.zip -Force
#
Copy-Item $scriptroot\doxygen.sty $latex
#Copy-Item $scriptroot\latex_header.tex $latex
cd $latex
./make
cd $parent
Copy-Item $latex\refman.pdf $download\tcIoc_documentation_${version}.pdf