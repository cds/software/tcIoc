// This is the implementation of ParseTcXml.
#include "ParseTmc.h"
#include "ParseUtilConst.h"
#include "ParseTcXmlConst.h"
//#define XML_STATIC ///< Static linking
#include "expat.h"

using namespace ParseUtil;

/** @file ParseTcXml.cpp
	Methods to parse TwinCAT tpy file.
 ************************************************************************/

namespace ParseTcXml {	

/* Forward declaration of XML callbacks
 ************************************************************************/
static void XMLCALL startElement (void *userData, const char *name, 
	const char **atts);
static void XMLCALL endElement (void *userData, const char *name);
static void XMLCALL startCData (void *userData) noexcept;
static void XMLCALL endCData (void *userData) noexcept;
static void XMLCALL dataElement (void *userData, const char *data, int len);


/* tmc_parserinfo::init
 ************************************************************************/
 void tmc_parserinfo::init() 
 {
	 sym = symbol_record();
	 rec = type_record();
	 name_parse = 0;
	 type_parse = 0;
	 opc_cur = 0;
	 opc_prop = property_el();
	 opc_parse = 0;
	 opc_data = std::stringcase ("");
	 opc_cdata = 0;
	 bitsize_parse = 0;
	 bitoffs_parse = 0;
	 data = std::stringcase ("");
	 array_parse = 0;
	 array_data = std::stringcase ("");
	 array_bounds = dimension (0, 0);
	 enum_parse = 0;
	 enum_data = std::stringcase ("");
	 enum_element = enum_pair (0, std::stringcase (""));
	 struct_parse = 0;
	 struct_element = item_record ();
	 fb_parse = 0;
 }

/* tmc_parserinfo::get_type_description
 ************************************************************************/
 type_enum tmc_parserinfo::get_type_description() const noexcept
 {
	 if (name_parse != 1) return type_enum::unknown;
	 if (array_parse == 1 && type_parse == 1) return type_enum::arraytype;
	 if (enum_parse == 1) return type_enum::enumtype;
	 if (struct_parse == 1 && !fb_parse) return type_enum::structtype;
	 if (struct_parse == 1 &&  fb_parse) return type_enum::functionblock;
	 if (type_parse == 1) return type_enum::simple;
	 return type_enum::unknown;
 }

 /* tc_xml_file::parse
 ************************************************************************/
 bool tmc_parser::parse_setup()
 {
	 info.get_projectinfo().set_tcat_versionstr("3.1.0.0");
	 info.get_projectinfo().set_cmpl_versionstr("3.1");
	 parser = XML_ParserCreate(NULL);
	 XML_SetUserData(parser, &info);
	 XML_SetElementHandler(parser, startElement, endElement);
	 XML_SetCdataSectionHandler(parser, startCData, endCData);
	 XML_SetCharacterDataHandler(parser, dataElement);
	 return true;
 }

 /* tc_xml_file::parse
  ************************************************************************/
 bool tmc_parser::parse_work(const char* p, int len, bool isFinal)
 {
	 if (!p || (len < 0)) {
		 return false;
	 }
	 // read data and parse
	 if (XML_Parse(parser, p, len, isFinal) == XML_Status::XML_STATUS_ERROR) {
		 fprintf(stderr,
			 "%s at line %llu\n",
			 XML_ErrorString(XML_GetErrorCode(parser)),
			 (unsigned long long)XML_GetCurrentLineNumber(parser));
		 return false;
	 }
	 return true;
 }

 /* tc_xml_file::parse_finish
  ************************************************************************/
 bool tmc_parser::parse_finish()
 {
	 // Finish up
	 XML_ParserFree(parser);
	 parser = nullptr;

	 // patch missing type decorators on types
	 int num = type_list.patch_tmc_type_decorators();

	 // patch missing type decorators on symbols
	 for (auto& i : sym_list) {
		 if (type_list.patch_tmc_type_decorators(i)) ++num; 
		 // patch arrays
		 type_list.patch_tmc_type_array(i, 8 * i.get_bytesize());
	 }
	 if (num > 0) {
		 // fprintf(stderr, "Patching %d type decorators\n", num);
	 }

	 // set plc name in opc
	 std::stringcase tcname = project_info.get_tc();
	 if (!tcname.empty()) {
		 for (auto& sym : sym_list) {
			 sym.get_opc().add(property_el(OPC_PROP_PLCNAME, tcname));
		 }
	 }
	 //// patch export all
	 //if (get_export_all()) {
	 //	for (auto& sym : sym_list) {
	 //		sym.get_opc().set_opc_state(ParseUtil::opc_enum::publish);
	 //	}
	 //}
	 return true;
 }

/************************************************************************/
/* XML Parsing
 ************************************************************************/

/** XML get namespace from attribute
 ************************************************************************/
static bool get_namespace (const char **atts, std::stringcase& ns)
{
	for (const char** pp = atts; pp && pp[0] && pp[1]; pp += 2) {
		std::stringcase a (pp[0]);
		if (a.compare (xmlAttrNamespace) == 0) {
			ns = pp[1];
			return true;
		}
	}
	return false;
}

/** XML get igroup number from attribute
 ************************************************************************/
static bool get_igroup(const char** atts, unsigned int& igroup)
{
	igroup = 0x4040;
	for (const char** pp = atts; pp && pp[0] && pp[1]; pp += 2) {
		std::stringcase a(pp[0]);
		if (a.compare(xmlAttrAreaType) == 0) {
			std::stringcase areatype = pp[1];
			if (areatype.compare(xmlAttrAreaTypeInternal) == 0) {
				igroup = 0x4040;
				return true;
			}
			else if (areatype.compare(xmlAttrAreaTypeInputDst) == 0) {
				igroup = 0xF020;
				return true;
			}
			else if (areatype.compare(xmlAttrAreaTypeOutputSrc) == 0) {
				igroup = 0xF030;
				return true;
			}
			return false;
		}
	}
	return false;
}

/** XML get pointer from attribute
 ************************************************************************/
static pointerref_enum get_pointer (const char **atts)
{
	for (const char** pp = atts; pp && pp[0] && pp[1]; pp += 2) {
		std::stringcase a (pp[0]);
		if (a.compare (xmlAttrPointerTo) == 0) {
			std::stringcase val (pp[1]);
			if ((val == "true") || (val == "t") || (val == "1")) return pointerref_enum::pointer;
		}
		else if (a.compare(xmlAttrReferenceTo) == 0) {
			std::stringcase val(pp[1]);
			if ((val == "true") || (val == "t") || (val == "1"))  return pointerref_enum::reference;
		}
	}
	return pointerref_enum::No;
}

/* XML start element function callback
 ************************************************************************/
static void XMLCALL startElement (void *userData, const char *name, 
	const char **atts)
{
	tmc_parserinfo*	pinfo = (tmc_parserinfo*) userData;
	if (pinfo->ignore) {
		++pinfo->ignore;
		return;
	}
	std::stringcase n (name ? name : "");
	//printf("%s\n", n.c_str());

	// Parse PLC project information
	if (pinfo->verytop() && (n.compare (xmlPlcProjectTmcInfo) == 0)) {
		pinfo->projects = true;
	}

	// array info
	else if (n.compare(xmlArrayInfo) == 0) {
		pinfo->array_parse = 2;
		pinfo->array_bounds = dimension(0, 0);
	}
	// lower array bound
	else if (n.compare(xmlArrayLBound) == 0 &&
		pinfo->array_parse == 2) {
		pinfo->array_parse = 3;
		pinfo->array_data = std::stringcase("");
	}
	// array elements
	else if (n.compare(xmlArrayElements) == 0 &&
		pinfo->array_parse == 2) {
		pinfo->array_parse = 3;
		pinfo->array_data = std::stringcase("");
	}

	// default values
	else if (n.compare(xmlDefault) == 0) {
		pinfo->default_parse = 2;
		pinfo->defvals = default_values{};
		pinfo->defval = basic_default_value_item{};
	}
	// default value sub item
	else if (n.compare(xmlDefaultSubItem) == 0 && pinfo->default_parse == 2) {
		pinfo->default_parse = 12;
		pinfo->defval = basic_default_value_item{};
	}
	// default value sub item name
	else if (n.compare(xmlDefaultSubItemName) == 0 && pinfo->default_parse == 12) {
		pinfo->default_parse = 18;
		pinfo->def_data = "";
	}
	// default int/real
	else if (n.compare(xmlDefaultValue) == 0 && pinfo->default_parse % 10 == 2) {
		pinfo->default_parse += 1;
		pinfo->def_data = "";
	}
	// default bool
	else if (n.compare(xmlDefaultBool) == 0 && pinfo->default_parse % 10 == 2) {
		pinfo->default_parse += 2;
		pinfo->def_data = "";
	}
	// default string
	else if (n.compare(xmlDefaultString) == 0 && pinfo->default_parse % 10 == 2) {
		pinfo->default_parse += 3;
		pinfo->def_data = "";
	}
	// default date/time
	else if (n.compare(xmlDefaultDateTime) == 0 && pinfo->default_parse % 10 == 2) {
		pinfo->default_parse += 4;
		pinfo->def_data = "";
	}
	// default enum text
	else if (n.compare(xmlDefaultEnumText) == 0 && pinfo->default_parse % 10 == 2) {
		pinfo->default_parse += 5;
		pinfo->def_data = "";
	}

	// Parse modules
	else if (n.compare(xmlModules) == 0) {
		if (pinfo->top()) {
			++pinfo->modules;
		}
		else ++pinfo->ignore;
	}
	// Parse module
	else if (n.compare(xmlModule) == 0) {
		if (pinfo->modules == 1) {
			++pinfo->modules;
		}
		else ++pinfo->ignore;
	}
	// Parse module name
	else if (n.compare(xmlName) == 0 && pinfo->modules == 2 && 
		     pinfo->dataarea == 0 && pinfo->properties == 0 && 
		     pinfo->name_parse == 0) {
		pinfo->name_parse = 2;
		pinfo->data = std::stringcase("");
	}
	// Parse module proprties
	else if (n.compare(xmlProperties) == 0 && pinfo->modules == 2 && 
		     pinfo->dataarea == 0 && pinfo->properties == 0 && 
		     pinfo->name_parse == 0) {
		++pinfo->properties;
	}
	// Parse module proprty 
	else if (n.compare(xmlProperty) == 0 && pinfo->modules == 2 && 
		     pinfo->dataarea == 0 && pinfo->properties == 1 && 
		     pinfo->name_parse == 0) {
		++pinfo->properties;
	}
	else if (pinfo->modules == 2 && pinfo->dataarea == 0 && 
		     pinfo->properties == 2 && pinfo->name_parse == 0) {
		// module property name
		if (n.compare(xmlName) == 0) {
			pinfo->name_parse = 2;
			pinfo->module_prop_name = "";
			pinfo->opc_cdata = 1;
		}
		// module property value
		else if (n.compare(xmlValue) == 0) {
			pinfo->value_parse = 2;
			pinfo->module_prop_value = "";
			pinfo->opc_cdata = 1;
		}
		else ++pinfo->ignore;		
	}
	// Parse data areas
	else if (n.compare(xmlDataAreas) == 0) {
		if (pinfo->modules == 2 && pinfo->dataarea == 0 && 
			pinfo->properties == 0) {
			++pinfo->dataarea;
		}
		else ++pinfo->ignore;
	}
	// Parse data area
	else if (n.compare(xmlDataArea) == 0) {
		if (pinfo->modules == 2 && pinfo->dataarea == 1 &&
			pinfo->properties == 0) {
			++pinfo->dataarea;
		}
		else ++pinfo->ignore;
	}
	// inside data area
	else if (pinfo->modules == 2 && pinfo->dataarea == 2) {

		// need to get area type
		if (n.compare(xmlAreaNo) == 0 && pinfo->symbols == 0) {
			// get igroup from attributes
			get_igroup(atts, pinfo->igroup);
			// don't care about the rest
			++pinfo->ignore;
		}
		// found a symbol
		else if (n.compare(xmlSymbol) == 0) {
			if (pinfo->symbols == 0) {
				pinfo->symbols = 2;
				pinfo->init();
			}
			else ++pinfo->ignore;
		}
		else if (pinfo->symbols == 2) {
			// name of symbol
			if (n.compare(xmlName) == 0 && !pinfo->name_parse &&
				!pinfo->opc_parse) {
				pinfo->name_parse = 2;
			}
			// type of symbol
			else if (n.compare(xmlBaseType) == 0 && !pinfo->type_parse) {
				pinfo->type_parse = 2;
				std::stringcase ns;
				if (get_namespace(atts, ns)) {
					pinfo->sym.set_type_namespace(ns);
				}
				pinfo->sym.set_type_pointer(get_pointer(atts));
			}
			// opc properties
			else if (n.compare(xmlProperties) == 0 && !pinfo->opc_parse &&
				pinfo->name_parse <= 1 && pinfo->type_parse <= 1) {
				pinfo->opc_parse = 1;
			}
			// opc property
			else if (n.compare(xmlProperty) == 0 && pinfo->opc_parse == 1) {
				pinfo->opc_parse = 2;
				pinfo->opc_prop = property_el();
			}
			// opc property name
			else if (n.compare(xmlName) == 0 && pinfo->opc_parse == 2) {
				pinfo->opc_parse = 3;
				pinfo->opc_data = "";
				pinfo->opc_cdata = 1;
			}
			// opc property value
			else if (n.compare(xmlValue) == 0 && pinfo->opc_parse == 2) {
				pinfo->opc_parse = 4;
				pinfo->opc_data = "";
				pinfo->opc_cdata = 1;
			}
			// bitoffs
			else if (n.compare(xmlBitOffs) == 0 && !pinfo->bitoffs_parse) {
				pinfo->bitoffs_parse = 2;
				pinfo->data = std::stringcase("");
			}
			// bitsize
			else if (n.compare(xmlBitSize) == 0 && !pinfo->bitsize_parse) {
				pinfo->bitsize_parse = 2;
				pinfo->data = std::stringcase("");
			}
			else {
				++pinfo->ignore;
			}
		}
		else {
			++pinfo->ignore;
		}
	}
	// Parse data type information
	else if (n.compare (xmlDataTypes) == 0) {
		if (pinfo->top()) {
			++pinfo->types;
		}
		else ++pinfo->ignore;
	}
	else if (n.compare (xmlDataType) == 0) {
		if (pinfo->types == 1) {
			++pinfo->types;
			pinfo->init();
		}
		else ++pinfo->ignore;
	}
	else if (pinfo->types == 2) {
		// name of type
		if (n.compare (xmlName) == 0 && !pinfo->name_parse && 
			(pinfo->struct_parse <= 1) && !pinfo->opc_cur) {
				pinfo->name_parse = 2;
				std::stringcase ns;
				if (get_namespace(atts, ns)) {
					pinfo->rec.set_namespace(ns);
				}
		}
		// right hand type
		else if (n.compare (xmlBaseType) == 0 && !pinfo->type_parse && 
			pinfo->struct_parse <= 1) {
				pinfo->type_parse = 2;
				std::stringcase ns;
				if (get_namespace(atts, ns)) {
					pinfo->rec.set_type_namespace(ns);
				}
				pinfo->rec.set_type_pointer(get_pointer(atts));
		}
		// bit size
		else if (n.compare (xmlBitSize) == 0 && !pinfo->bitsize_parse &&
			pinfo->struct_parse <= 1) {
				pinfo->bitsize_parse = 2;
				pinfo->data = std::stringcase ("");
		}
		// enum
		else if (n.compare (xmlEnumInfo) == 0) {
			pinfo->enum_parse = 2;
			pinfo->enum_element = enum_pair (0, "");
			pinfo->enum_comment.clear();
		}
		// enum tag
		else if (n.compare (xmlEnumEnum) == 0 && 
			pinfo->enum_parse == 2) {
				pinfo->enum_parse = 3;
				pinfo->enum_data = std::stringcase ("");
		}
		// enum text
		else if (n.compare (xmlEnumText) == 0 && 
			pinfo->enum_parse == 2) {
				pinfo->enum_parse = 3;
				pinfo->enum_data = std::stringcase ("");
		}
		// enum comment
		else if (n.compare (xmlEnumComment) == 0 && 
			pinfo->enum_parse == 2) {
				pinfo->enum_parse = 3;
				pinfo->enum_data = std::stringcase ("");
		}
		// structure
		else if (n.compare (xmlSubItem) == 0) {
			pinfo->struct_parse = 2;
			pinfo->struct_element = item_record();
		}
		// structure element name
		else if (n.compare (xmlName) == 0 && pinfo->struct_parse == 2 && 
			!pinfo->opc_cur) {
				pinfo->struct_parse = 3;
		}
		// structure element type
		else if (n.compare (xmlType) == 0 && pinfo->struct_parse == 2 && 
			!pinfo->opc_cur) {
				pinfo->struct_parse = 4;
				std::stringcase ns;
				if (get_namespace(atts, ns)) {
					pinfo->struct_element.set_type_namespace(ns);
				}
				pinfo->struct_element.set_type_pointer(get_pointer(atts));
		}
		// structure element bitsize
		else if (n.compare (xmlBitSize) == 0 && pinfo->struct_parse == 2 && 
			!pinfo->opc_cur) {
				pinfo->struct_parse = 5;
				pinfo->data = std::stringcase ("");
		}
		// structure element bitoffs
		else if (n.compare (xmlBitOffs) == 0 && pinfo->struct_parse == 2 && 
			!pinfo->opc_cur) {
				pinfo->struct_parse = 5;
				pinfo->data = std::stringcase ("");
		}
		// function block
		else if (n.compare (xmlFbInfo) == 0) {
			pinfo->fb_parse = 1;
			++pinfo->ignore;
		}
		// opc properties
		else if (n.compare (xmlProperties) == 0 && !pinfo->opc_cur &&
			pinfo->name_parse <= 1 &&
			pinfo->type_parse <= 1 &&
			pinfo->enum_parse <= 1 &&
			pinfo->array_parse <= 1 &&
			pinfo->struct_parse <= 2) {
				pinfo->opc_parse = 1;
				if (pinfo->struct_parse == 2) 
					pinfo->opc_cur = &pinfo->struct_element.get_opc();
				else pinfo->opc_cur = &pinfo->rec.get_opc();
		}
		// opc property
		else if (n.compare (xmlProperty) == 0 && pinfo->opc_cur &&
			pinfo->opc_parse == 1) {
				pinfo->opc_parse = 2;
				pinfo->opc_prop = property_el ();
		}
		// opc property name
		else if (n.compare (xmlName) == 0 && pinfo->opc_cur &&
			pinfo->opc_parse == 2) {
				pinfo->opc_parse = 3;
				pinfo->opc_data = "";
				pinfo->opc_cdata = 1;
		}
		// opc property value
		else if (n.compare (xmlValue) == 0 && pinfo->opc_cur &&
			pinfo->opc_parse == 2) {
				pinfo->opc_parse = 4;
				pinfo->opc_data = "";
				pinfo->opc_cdata = 1;
		}
		else {
			++pinfo->ignore;
		}
	}
	else {
		++pinfo->ignore;
	}
}

/* XML end element function callback
 ************************************************************************/
static void XMLCALL endElement (void *userData, const char *name)
{
	tmc_parserinfo*	pinfo = (tmc_parserinfo*) userData;
	if (pinfo->ignore) {
		--pinfo->ignore;
		return;
	}
	std::stringcase n (name ? name : "");

	// parsing PLC project information
	if (n.compare (xmlPlcProjectTmcInfo) == 0) {
		if (pinfo->top()) pinfo->projects = false;
	}

	// parsed an array info
	else if (n.compare(xmlArrayInfo) == 0 && pinfo->array_parse == 2) {
		pinfo->array_parse = 1;
		// array on symbols
		if (pinfo->symbols == 2) {
			if (pinfo->array_bounds.second > 0) {
				pinfo->sym.get_array_dimensions().push_back(pinfo->array_bounds);
			}
		}
		// array on type 
		if (pinfo->types == 2) {
			// array on sub item 
			if (pinfo->struct_parse >= 2) {
				pinfo->struct_element.get_array_dimensions().push_back(pinfo->array_bounds);
			}
			else {
				if (pinfo->array_bounds.second > 0) {
					pinfo->rec.get_array_dimensions().push_back(pinfo->array_bounds);
				}
			}
		}
	}
	// lower array bound
	else if (n.compare(xmlArrayLBound) == 0 &&
		pinfo->array_parse == 3) {
		pinfo->array_bounds.first =
			strtol(pinfo->array_data.c_str(), NULL, 10);
		pinfo->array_parse = 2;
	}
	// array elements
	else if (n.compare(xmlArrayElements) == 0 &&
		pinfo->array_parse == 3) {
		pinfo->array_bounds.second =
			strtol(pinfo->array_data.c_str(), NULL, 10);
		pinfo->array_parse = 2;
	}

	// default values
	else if (n.compare(xmlDefault) == 0 && pinfo->default_parse == 2) {
		// got new default values
		if (pinfo->defvals.get_type() != default_enum::deflist) {
			pinfo->defvals.set_type(pinfo->defval.get_type());
			pinfo->defvals.set_value(pinfo->defval.get_value());
		}
		// check for array
		else if (!pinfo->defvals.get_list().empty()) {
			std::regex arrayindex{R"(\[[-+]?\d+\])"};
			bool isarray = true;
			default_enum arrtype = pinfo->defvals.get_list()[0].get_type();
			for (auto& i : pinfo->defvals.get_list()) {
				if (i.get_type() != arrtype || !std::regex_match (i.get_name(), arrayindex)) {
					isarray = false;
					break;
				}
			}
			if (isarray) {
				pinfo->defvals.set_array_type(arrtype);
			}
		}
		// check type is valid, then add
		if (pinfo->defvals.get_type() != default_enum::invalid) {
			// defaull value on symbols
			if (pinfo->symbols == 2) {
				pinfo->sym.set_defvalues((std::make_shared<default_values>(std::move(pinfo->defvals))));
			}
			// array on type 
			if (pinfo->types == 2) {
				// array on sub item 
				if (pinfo->struct_parse >= 2) {
					pinfo->struct_element.set_defvalues((std::make_shared<default_values>(std::move(pinfo->defvals))));
				}
				else {
					if (pinfo->array_bounds.second > 0) {
						pinfo->rec.set_defvalues((std::make_shared<default_values>(std::move(pinfo->defvals))));
					}
				}
			}
		}
		pinfo->default_parse = 1;
	}
	// default value sub item
	else if (n.compare(xmlDefaultSubItem) == 0 && pinfo->default_parse == 12) {
		// got new default subitem
		pinfo->defvals.set_type(default_enum::deflist);
		if (!pinfo->defval.get_name().empty()) {
			pinfo->defvals.get_list().push_back(pinfo->defval);
		}
		pinfo->default_parse = 2;
	}
	// default value sub item name
	else if (n.compare(xmlDefaultSubItemName) == 0 && pinfo->default_parse == 18) {
		// got new default subitem name
		trim_space(pinfo->def_data);
		pinfo->defval.set_name(pinfo->def_data);
		pinfo->default_parse = 12;
	}
	// default int/real
	else if (n.compare(xmlDefaultValue) == 0 && pinfo->default_parse % 10 == 3) {
		// got new default value int/real
		trim_space(pinfo->def_data);
		pinfo->defval.set_type(default_enum::defvalue);
		pinfo->defval.set_value(pinfo->def_data);
		pinfo->default_parse -= 1;
	}
	// default bool
	else if (n.compare(xmlDefaultBool) == 0 && pinfo->default_parse % 10 == 4) {
		// got new default value bool
		trim_space(pinfo->def_data);
		pinfo->defval.set_type(default_enum::defbool);
		pinfo->defval.set_value(pinfo->def_data);
		pinfo->default_parse -= 2;
	}
	// default string
	else if (n.compare(xmlDefaultString) == 0 && pinfo->default_parse % 10 == 5) {
		// got new default value string
		trim_space(pinfo->def_data);
		pinfo->defval.set_type(default_enum::defstring);
		pinfo->defval.set_value(pinfo->def_data);
		pinfo->default_parse -= 3;
	}
	// default date/time
	else if (n.compare(xmlDefaultDateTime) == 0 && pinfo->default_parse % 10 == 6) {
		// got new default value date/time
		trim_space(pinfo->def_data);
		pinfo->defval.set_type(default_enum::defdatatime);
		pinfo->defval.set_value(pinfo->def_data);
		pinfo->default_parse -= 4;
	}
	// default enum text
	else if (n.compare(xmlDefaultEnumText) == 0 && pinfo->default_parse % 10 == 7) {
		// got new default enum text
		trim_space(pinfo->def_data);
		pinfo->defval.set_type(default_enum::defenumtext);
		pinfo->defval.set_value(pinfo->def_data);
		pinfo->default_parse -= 5;
	}

	// Parsed modules
	else if (n.compare(xmlModules) == 0) {
		if (pinfo->modules == 1) --pinfo->modules;
	}
	// Parsed module
	else if (n.compare(xmlModule) == 0) {
		if (pinfo->modules == 2) --pinfo->modules;
	}
	// Parsed module name
	else if (n.compare(xmlName) == 0 && pinfo->modules == 2 && 
		pinfo->dataarea == 0 && pinfo->properties == 0) {
		if (pinfo->name_parse == 2) {
			pinfo->name_parse = 0;
			// set target name
			trim_space(pinfo->data);
			pinfo->get_projectinfo().set_targetname(pinfo->data);
		}
	}
	// Parsed module proprties
	else if (n.compare(xmlProperties) == 0 && pinfo->modules == 2 &&
		pinfo->dataarea == 0) {
		if (pinfo->properties == 1) --pinfo->properties;
	}
	// Parsed module proprty 
	else if (n.compare(xmlProperty) == 0 && pinfo->modules == 2 &&
		pinfo->dataarea == 0) {
		if (pinfo->properties == 2) {
			--pinfo->properties;
			// check property
			trim_space(pinfo->module_prop_name);
			trim_space(pinfo->module_prop_value);
			if (pinfo->module_prop_name.compare(xmlPropertyApplicationName) == 0) {
				static const std::regex findport (R"(Port_(\d+))", std::regex::icase);
				std::cmatch match;
				if (std::regex_match(pinfo->module_prop_value.c_str(), match, findport) &&
					match.size() >= 2) {
					long port = strtol(match[1].str().c_str(), NULL, 10);
					pinfo->get_projectinfo().set_port(port);
				}
			}
			pinfo->module_prop_name.clear();
			pinfo->module_prop_value.clear();
		}
	}
	else if (pinfo->modules == 2 && pinfo->dataarea == 0 &&
		pinfo->properties == 2) {
		// module property name
		if (n.compare(xmlName) == 0) {
			pinfo->name_parse = 0;
		}
		// module property value
		else if (n.compare(xmlValue) == 0) {
			pinfo->value_parse = 0;
		}
	}
	// Parsed data areas
	else if (n.compare(xmlDataAreas) == 0) {
		if (pinfo->modules == 2 && pinfo->dataarea == 1) {
			--pinfo->dataarea;
			pinfo->init();
		}
	}
	// Parsed data area
	else if (n.compare(xmlDataArea) == 0 && pinfo->modules == 2) {
		if (pinfo->modules == 2 && pinfo->dataarea == 2) --pinfo->dataarea;
	}
	// inside data area
	else if (pinfo->modules == 2 && pinfo->dataarea == 2) {

		// parsing symbols
		if (n.compare(xmlSymbol) == 0) {
			if (pinfo->symbols == 2) {
				pinfo->symbols = 0;
				pinfo->sym.set_igroup(pinfo->igroup); // set igroup here
				if (!pinfo->sym.get_name().empty()) {
					// pointers are readonly!
					if (pinfo->sym.get_type_pointer() != pointerref_enum::No) {
						pinfo->sym.get_opc().get_properties()[OPC_PROP_RIGHTS] = "1";
					}
					pinfo->get_symbols().push_back(pinfo->sym);
				}
			}
		}
		else if (pinfo->symbols == 2) {
			// parsed a name (trim space)
			if (n.compare(xmlName) == 0 && pinfo->name_parse == 2) {
				pinfo->name_parse = 1;
				trim_space(pinfo->sym.get_name());
			}
			// parsed a type (trim space)
			else if (n.compare(xmlBaseType) == 0 && pinfo->type_parse == 2) {
				pinfo->type_parse = 1;
				trim_space(pinfo->sym.get_type_name());
			}
			// opc properties
			else if (n.compare(xmlProperties) == 0 && pinfo->opc_parse == 1) {
				pinfo->opc_parse = 0;
			}
			// opc property
			else if (n.compare(xmlProperty) == 0 && pinfo->opc_parse == 2) {
				pinfo->opc_parse = 1;
				if (pinfo->opc_prop.first == -1) {
					const int num = strtol(pinfo->opc_prop.second.c_str(), NULL, 10);
					pinfo->sym.get_opc().set_opc_state(num ? opc_enum::publish : opc_enum::silent);
				}
				else if (pinfo->opc_prop.first > 0) {
					pinfo->sym.get_opc().add(pinfo->opc_prop);
				}
			}
			// opc property name
			else if (n.compare(xmlName) == 0 && pinfo->opc_parse == 3) {
				pinfo->opc_parse = 2;
				trim_space(pinfo->opc_data);
				int num = 0;
				if (pinfo->opc_data.compare(opcExport) == 0) num = -1;
				else if (pinfo->opc_data.compare(0, 8, opcProp) == 0) {
					pinfo->opc_data.erase(0, 8);
					trim_space(pinfo->opc_data);
					if (pinfo->opc_data.compare(0, 1, opcBracket) == 0)
						pinfo->opc_data.erase(0, 1);
					num = strtol(pinfo->opc_data.c_str(), NULL, 10);
				}
				pinfo->opc_prop.first = num;
			}
			// opc property value
			else if (n.compare(xmlValue) == 0 && pinfo->opc_parse == 4) {
				pinfo->opc_parse = 2;
				pinfo->opc_prop.second = pinfo->opc_data;
			}
			// parsed a bitoffs (convert to integer)
			else if (n.compare(xmlBitOffs) == 0 && pinfo->bitoffs_parse == 2) {
				pinfo->bitoffs_parse = 1;
				pinfo->sym.set_ioffset(strtol(pinfo->data.c_str(), NULL, 10) / 8);
			}
			// parsed a bitsize (convert to integer)
			else if (n.compare(xmlBitSize) == 0 && pinfo->bitsize_parse == 2) {
				pinfo->bitsize_parse = 1;
				pinfo->sym.set_bytesize(strtol(pinfo->data.c_str(), NULL, 10) / 8);
			}
		}
	}
	// parsing data types
	else if (n.compare (xmlDataTypes) == 0) {
		if (pinfo->types == 1) --pinfo->types;
	}
	else if (n.compare (xmlDataType) == 0) {
		if (pinfo->types == 2) {
			--pinfo->types;
			pinfo->rec.set_type_description (pinfo->get_type_description());
			// remove simple type which reference themselves, ie., discard type aliases
			if ((pinfo->rec.get_type_description() != type_enum::simple) ||
				(pinfo->rec.get_name() != pinfo->rec.get_type_name())) {
				unsigned int decoration = pinfo->rec.get_name_decoration();
				if (decoration == 0) decoration = (unsigned int)pinfo->get_types().size() + 1;
				pinfo->rec.set_name_decoration(decoration);
				pinfo->get_types().insert(type_map::value_type (decoration, pinfo->rec));
			}
		}
	}
	else if (pinfo->types == 2) {
		// parsed a name (trim space)
		if (n.compare (xmlName) == 0 && pinfo->name_parse == 2) {
			pinfo->name_parse = 1;
			trim_space (pinfo->rec.get_name());
		}
		// parsed a type (trim space)
		else if (n.compare (xmlBaseType) == 0 && pinfo->type_parse == 2) {
			pinfo->type_parse = 1;
			trim_space (pinfo->rec.get_type_name());
		}
		// parsed a bit size
		else if (n.compare (xmlBitSize) == 0 && pinfo->bitsize_parse == 2) {
			pinfo->bitsize_parse = 1;
			pinfo->rec.set_bit_size (strtol (pinfo->data.c_str(), NULL, 10));
		}
		// parsed an enum info
		else if (n.compare (xmlEnumInfo) == 0 && pinfo->enum_parse == 2) {
			pinfo->enum_parse = 1;
			pinfo->rec.get_enum_list().insert (pinfo->enum_element);
			if (!pinfo->enum_comment.empty() && (pinfo->enum_element.first >= 0) &&
			   (pinfo->enum_element.first < 16)) {
				pinfo->rec.get_opc().get_properties()[OPC_PROP_ZRST+pinfo->enum_element.first] = pinfo->enum_comment;
			}
		}
		// enum tag
		else if (n.compare (xmlEnumEnum) == 0 && 
			pinfo->enum_parse == 3) {
				pinfo->enum_element.first = 
					strtol (pinfo->enum_data.c_str(), NULL, 10);
				pinfo->enum_parse = 2;
		}
		// enum text
		else if (n.compare (xmlEnumText) == 0 && 
			pinfo->enum_parse == 3) {
				trim_space (pinfo->enum_data);
				pinfo->enum_element.second = pinfo->enum_data;
				pinfo->enum_parse = 2;
		}
		// enum comment
		else if (n.compare (xmlEnumComment) == 0 && 
			pinfo->enum_parse == 3) {
				trim_space (pinfo->enum_data);
				pinfo->enum_comment = pinfo->enum_data;
				pinfo->enum_parse = 2;
		}
		// parsed a subitem
		else if (n.compare (xmlSubItem) == 0 && pinfo->struct_parse == 2) {
			pinfo->struct_parse = 1;
			pinfo->rec.get_struct_list().push_back (pinfo->struct_element);
		}
		// subitem name
		else if (n.compare (xmlName) == 0 && pinfo->struct_parse == 3) {
			trim_space (pinfo->struct_element.get_name());
			pinfo->struct_parse = 2;
		}
		// subitem type
		else if (n.compare (xmlType) == 0 && pinfo->struct_parse == 4) {
			trim_space (pinfo->struct_element.get_type_name());
			pinfo->struct_parse = 2;
		}
		// subitem bitsize
		else if (n.compare (xmlBitSize) == 0 && pinfo->struct_parse == 5) {
			pinfo->struct_element.set_bit_size (
				strtol (pinfo->data.c_str(), NULL, 10));
			pinfo->struct_parse = 2;
		}
		// subitem bitoffs
		else if (n.compare (xmlBitOffs) == 0 && pinfo->struct_parse == 5) {
			pinfo->struct_element.set_bit_offset (
				strtol (pinfo->data.c_str(), NULL, 10));
			pinfo->struct_parse = 2;
		}
		// opc properties
		else if (n.compare (xmlProperties) == 0 && pinfo->opc_parse == 1) {
			pinfo->opc_parse = 0;
			pinfo->opc_cur = 0;
		}
		// opc property
		else if (n.compare (xmlProperty) == 0 && pinfo->opc_parse == 2) {
			pinfo->opc_parse = 1;
			if (pinfo->opc_prop.first == -1) {
				const int num = strtol (pinfo->opc_prop.second.c_str(), NULL, 10);
				if (pinfo->opc_cur) 
					pinfo->opc_cur->set_opc_state (num ? opc_enum::publish : opc_enum::silent);
			}
			else if (pinfo->opc_prop.first > 0) {
				if (pinfo->opc_cur) 
					pinfo->opc_cur->get_properties().insert (pinfo->opc_prop);
			}
		}
		// opc property name
		else if (n.compare (xmlName) == 0 && pinfo->opc_parse == 3) {
			pinfo->opc_parse = 2;
			trim_space (pinfo->opc_data);
			int num = 0;
			if (pinfo->opc_data.compare (opcExport) == 0) num = -1;
			else if (pinfo->opc_data.compare (0, 8, opcProp) == 0) {
				pinfo->opc_data.erase (0, 8);
				trim_space (pinfo->opc_data);
				if (pinfo->opc_data.compare (0, 1, opcBracket) == 0) 
					pinfo->opc_data.erase (0, 1);
				num = strtol (pinfo->opc_data.c_str(), NULL, 10);
			}
			pinfo->opc_prop.first = num;
		}
		// opc property value
		else if (n.compare (xmlValue) == 0 && pinfo->opc_parse == 4) {
			pinfo->opc_parse = 2;
			pinfo->opc_prop.second = pinfo->opc_data;
		}
	}
}

/* XML start CData function callback
 ************************************************************************/
static void XMLCALL startCData (void *userData) noexcept
{
	tmc_parserinfo*	pinfo = (tmc_parserinfo*) userData;
	if (pinfo->ignore) {
		return;
	}
	if (pinfo->symbols == 2) {
		// clear opc data
		if (pinfo->opc_parse >= 3) {
			pinfo->opc_data.clear ();
			pinfo->opc_cdata = 1;
		}
	}
	else if (pinfo->types == 2) {
		// clear opc data
		if (pinfo->opc_parse >= 3) {
			pinfo->opc_data.clear ();
			pinfo->opc_cdata = 1;
		}
	}
}

/* XML end CData function callback
 ************************************************************************/
static void XMLCALL endCData (void *userData) noexcept
{
	tmc_parserinfo*	pinfo = (tmc_parserinfo*) userData;
	if (pinfo->ignore) {
		return;
	}
	if (pinfo->symbols == 2) {
		// clear opc data
		if (pinfo->opc_parse >= 3) {
			pinfo->opc_cdata = 0;
		}
	}
	else if (pinfo->types == 2) {
		// clear opc data
		if (pinfo->opc_parse >= 3) {
			pinfo->opc_cdata = 0;
		}
	}
}


/* XML data function callback
 ************************************************************************/
static void XMLCALL dataElement (void *userData, const char *data, int len)
{
	tmc_parserinfo*	pinfo = (tmc_parserinfo*) userData;
	if (pinfo->ignore) {
		return;
	}

	// append array info data
	else if (pinfo->array_parse == 3) {
		pinfo->array_data.append(data, len);
	}

	// append default value data
	else if (pinfo->default_parse % 10 >= 3) {
		pinfo->def_data.append(data, len);
	}

	// get symbol data
	else if (pinfo->symbols == 2) {
		// append string to name
		if (pinfo->name_parse == 2) {
			pinfo->sym.get_name().append (data, len);
		}
		// append string to type
		else if (pinfo->type_parse == 2) {
			pinfo->sym.get_type_name().append (data, len);
		}
		// append opc data
		else if (pinfo->opc_parse >= 3) {
			if (pinfo->opc_cdata) 
				pinfo->opc_data.append (data, len);
		}
		// append igroup/ioffset/bitsize data
		else if ((pinfo->bitoffs_parse == 2) ||
			(pinfo->bitsize_parse == 2)) {
				pinfo->data.append (data, len);
		}
	}

	// get data type information
	else if (pinfo->types == 2) {
		// append string to name
		if (pinfo->name_parse == 2) {
			pinfo->rec.get_name().append (data, len);
		}
		// append string to type
		else if (pinfo->type_parse == 2) {
			pinfo->rec.get_type_name().append (data, len);
		}
		// append string to bitdata
		else if (pinfo->bitsize_parse == 2) {
			pinfo->data.append (data, len);
		}
		// append enum data
		else if (pinfo->enum_parse == 3) {
			pinfo->enum_data.append (data, len);
		}
		// append struct element name
		else if (pinfo->struct_parse == 3) {
			pinfo->struct_element.get_name().append (data, len);
		}
		// append struct element type
		else if (pinfo->struct_parse == 4) {
			pinfo->struct_element.get_type_name().append (data, len);
		}
		// append struct element bitsize
		else if (pinfo->struct_parse == 5) {
			pinfo->data.append (data, len);
		}
		// append struct element bitoffs
		else if (pinfo->struct_parse == 6) {
			pinfo->data.append (data, len);
		}
		// append opc data
		else if (pinfo->opc_parse >= 3) {
			if (pinfo->opc_cdata) 
				pinfo->opc_data.append (data, len);
		}
	}
	// get module name
	else if (pinfo->modules == 2 && pinfo->dataarea == 0 &&
		pinfo->properties == 0 && pinfo->name_parse == 2) {
			pinfo->data.append(data, len);
	}
	// get module property
	else if (pinfo->modules == 2 && pinfo->dataarea == 0 &&	pinfo->properties == 2) {
		if (pinfo->name_parse == 2) {
			pinfo->module_prop_name.append(data, len);
		}
		else if (pinfo->value_parse == 2) {
			pinfo->module_prop_value.append(data, len);
		}
	}
}

}